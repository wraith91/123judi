<?php

use App\Agenwin\TransactionStatus;
use Illuminate\Database\Seeder;

class TransactionStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transaction_status')->delete();

        TransactionStatus::create([
            'name' => 'All'
        ]);

        TransactionStatus::create([
        	'name' => 'Pending'
        ]);

        TransactionStatus::create([
        	'name' => 'Approved'
        ]);

        TransactionStatus::create([
        	'name' => 'Rejected'
        ]);

        TransactionStatus::create([
            'name' => 'To Be Approve'
        ]);

        TransactionStatus::create([
            'name' => 'Processing'
        ]);

        TransactionStatus::create([
        	'name' => 'Cancelled'
        ]);
    }
}
