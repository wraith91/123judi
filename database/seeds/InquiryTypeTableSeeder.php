<?php

use App\Agenwin\InquiryType;

use Illuminate\Database\Seeder;

class InquiryTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inquiry_types')->delete();

        InquiryType::create([
					'name'        => 'Inquiry'
        ]);

        InquiryType::create([
					'name'        => 'Feedback'
        ]);

        InquiryType::create([
					'name'        => 'Complaint'
        ]);
    }
}

