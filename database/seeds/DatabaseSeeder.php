<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::statement("SET foreign_key_checks = 0");

        Model::unguard();

        $this->call(BankTypeTableSeeder::class);        
        $this->call(GameTypeTableSeeder::class);        
        $this->call(InquiryTypeTableSeeder::class);        
        $this->call(InquiryStatusTableSeeder::class);        
        $this->call(TransactionTypeTableSeeder::class);        
        $this->call(TransactionStatusTableSeeder::class); 
        $this->call(RolesAndPermissionTableSeeder::class); 
        $this->call(UserTableSeeder::class); 
        $this->call(BonusStatusTableSeeder::class);

        Model::reguard();
    }
}
