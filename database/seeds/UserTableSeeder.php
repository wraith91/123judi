<?php

use Illuminate\Database\Seeder;
use App\Agenwin\User;
use App\Agenwin\UserProfile;
use App\Agenwin\UserBankProfile;
use App\Agenwin\UserGameProfile;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('users')->delete();

       	$user = new User([
            'username'  => 'ironman',
            'password'  => 'qweasdzxc',
            'email'     => 'tony@stark.com',
        ]);

        $profile = new UserProfile([
            'first_name' => 'Tony',
            'last_name'  => 'Stark',
            'birth_date'  => '2015-11-15',
        ]);

        $bank = new UserBankProfile([
            'bank_type_id' => 3,
            'account_name' => 'Tony Stark',
            'account_no'   => '9282193892',
        ]);

        $game = new UserGameProfile([
            'game_id' => 1,
            'username' => 'sbo0001',
            'password' => '12345', 
        ]);

        $user->save();

        $profile = $user->profile()->save($profile);

        $bank = $user->bankAccount()->save($bank);

        $game = $user->gameAccount()->save($game);

        $user->assignRole('member');

        $user = new User([
            'username'  => 'antman',
            'password'  => 'qweasdzxc',
            'email'     => 'scott@lang.com',
        ]);

        $profile = new UserProfile([
            'first_name' => 'Scott',
            'last_name'  => 'Lang',
            'birth_date'  => '2015-11-15',
        ]);

        $bank = new UserBankProfile([
            'bank_type_id' => 3,
            'account_name' => 'Scott Lang',
            'account_no'   => '9282393822',
        ]);

        $game = new UserGameProfile([
            'game_id' => 1,
            'username' => 'sbo0002',
            'password' => '12345', 
        ]);

        $user->save();

        $profile = $user->profile()->save($profile);

        $bank = $user->bankAccount()->save($bank);

        $game = $user->gameAccount()->save($game);

        $user->assignRole('member');

        $user = new User([
            'username'  => 'hulk',
            'password'  => 'qweasdzxc',
            'email'     => 'banner@bruce.com',
        ]);

        $profile = new UserProfile([
            'first_name' => 'Bruce',
            'last_name'  => 'Banner',
            'birth_date'  => '2015-11-15',
        ]);

        $bank = new UserBankProfile([
            'bank_type_id' => 3,
            'account_name' => 'Bruce Banner',
            'account_no'   => '9282391892',
        ]);

        $game = new UserGameProfile([
            'game_id' => 1,
            'username' => 'sbo0003',
            'password' => '12345', 
        ]);

        $user->save();

        $profile = $user->profile()->save($profile);

        $bank = $user->bankAccount()->save($bank);

        $game = $user->gameAccount()->save($game);

        $user->assignRole('member');

        // -----------------------------------------------------------------------

        $user = new User([
            'username'  => 'tchalla',
            'password'  => 'qweasdzxc',
            'email'     => 'black@panther.com',
        ]);

        $profile = new UserProfile([
            'first_name' => 'Black',
            'last_name'  => 'Panther',
            'birth_date'  => '2015-11-15',
        ]);

        $bank = new UserBankProfile([
            'bank_type_id' => 3,
            'account_name' => 'Chala',
            'account_no'   => '9282393592',
        ]);

        $game = new UserGameProfile([
            'game_id' => 2,
            'username' => 'ibc0001',
            'password' => '12345', 
        ]);

        $user->save();

        $profile = $user->profile()->save($profile);

        $bank = $user->bankAccount()->save($bank);

        $game = $user->gameAccount()->save($game);

        $user->assignRole('member');

        $user = new User([
            'username'  => 'hawkeye',
            'password'  => 'qweasdzxc',
            'email'     => 'clinton@barton.com',
        ]);

        $profile = new UserProfile([
            'first_name' => 'Clinton',
            'last_name'  => 'Barton',
            'birth_date'  => '2015-11-15',
        ]);

        $bank = new UserBankProfile([
            'bank_type_id' => 3,
            'account_name' => 'Clinton Barton',
            'account_no'   => '6282393892',
        ]);

        $game = new UserGameProfile([
            'game_id' => 2,
            'username' => 'ibc0002',
            'password' => '12345', 
        ]);

        $user->save();

        $profile = $user->profile()->save($profile);

        $bank = $user->bankAccount()->save($bank);

        $game = $user->gameAccount()->save($game);

        $user->assignRole('member');

        $user = new User([
            'username'  => 'cage',
            'password'  => 'qweasdzxc',
            'email'     => 'luke@cage.com',
        ]);

        $profile = new UserProfile([
            'first_name' => 'Luke',
            'last_name'  => 'Cage',
            'birth_date'  => '2015-11-15',
        ]);

        $bank = new UserBankProfile([
            'bank_type_id' => 3,
            'account_name' => 'Luke Cage',
            'account_no'   => '9282398892',
        ]);

        $game = new UserGameProfile([
            'game_id' => 2,
            'username' => 'ibc0003',
            'password' => '12345', 
        ]);

        $user->save();

        $profile = $user->profile()->save($profile);

        $bank = $user->bankAccount()->save($bank);

        $game = $user->gameAccount()->save($game);

        $user->assignRole('member');




        $user = new User([
            'username'  => 'wasp',
            'password'  => 'qweasdzxc',
            'email'     => 'pym@janet.com',
        ]);

        $profile = new UserProfile([
            'first_name' => 'Janet',
            'last_name'  => 'Pym',
            'birth_date'  => '2015-11-15',
        ]);

        $bank = new UserBankProfile([
            'bank_type_id' => 3,
            'account_name' => 'Janet Pym',
            'account_no'   => '9282373892',
        ]);

        $game = new UserGameProfile([
            'game_id' => 3,
            'username' => 'cali0001',
            'password' => '12345', 
        ]);

        $user->save();

        $profile = $user->profile()->save($profile);

        $bank = $user->bankAccount()->save($bank);

        $game = $user->gameAccount()->save($game);

        $user->assignRole('member');

        $user = new User([
            'username'  => 'quicksilver',
            'password'  => 'qweasdzxc',
            'email'     => 'maximoff@pietro.com',
        ]);

        $profile = new UserProfile([
            'first_name' => 'Pietro',
            'last_name'  => 'Maximoff',
            'birth_date'  => '2015-11-15',
        ]);

        $bank = new UserBankProfile([
            'bank_type_id' => 3,
            'account_name' => 'Pietro Maximoff',
            'account_no'   => '1282373892',
        ]);

        $game = new UserGameProfile([
            'game_id' => 3,
            'username' => 'cali0002',
            'password' => '12345', 
        ]);

        $user->save();

        $profile = $user->profile()->save($profile);

        $bank = $user->bankAccount()->save($bank);

        $game = $user->gameAccount()->save($game);

        $user->assignRole('member');








        $admin = User::create([
            'username'  => 'admin',
            'password'  => 'qweqweqwe',
            'email'     => 'admin@agenwin.com',
        ])->assignRole('admin');

        $adminProfile = new UserProfile([
        		'user_id' => $admin->id,
            'first_name' => 'Web',
            'last_name'  => 'Administrator',
        ]);

        $admin = User::create([
            'username'  => 'root',
            'password'  => 'qwertyfgh',
            'email'     => 'admin2@agenwin.com',
        ])->assignRole('admin');

        $adminProfile = new UserProfile([
                'user_id' => $admin->id,
            'first_name' => 'Saitama',
            'last_name'  => 'Administrator',
        ]);
    }
}
