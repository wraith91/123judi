@if ($paginator->lastPage() > 1)
<div class="pagination taCenter">
    <a class="pagination-anchor {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}" href="{{ $paginator->url(1) }}"  style="color: black;">
        &lt;
    </a>        
    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        <a class="pagination-anchor {{ ($paginator->currentPage() == $i) ? ' current-page' : '' }}" href="{{ $paginator->url($i) }}"  style="color: black;">
            {{ $i }}
        </a>
    @endfor
    <a class="pagination-anchor {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}" href="{{ $paginator->url($paginator->currentPage()+1) }}"  style="color: black;">
        &gt;
    </a>      
</div><!-- pagination -->
@endif