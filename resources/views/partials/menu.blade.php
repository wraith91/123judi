<nav class="cf">
    <div class="main-nav">
        <ul class="inner-main-nav cf">
            <li class="{{ set_active('/') }}" ><a href="/">Home</a></li>
            <li class="{{ set_active('casino') }}"><a href="/casino">Casino</a></li>
            <li class="{{ set_active('poker') }}" ><a href="/poker">Poker</a></li>
            <li class="{{ set_active('help') }}" ><a href="/help">Help Center</a></li>
            <li class="{{ set_active('contact') }}" ><a href="/contact">Hubungi Kami</a></li>
            <li><a href="http://9bet.me/body/promo.html" target="_blank">Promosi</a></li>
            <li><a href="http://www.9bet.me/body/9betangels.html" target="_blank">9Angels</a></li>
            @if (Auth::guest()) 
                <li><a href="/auth/login">Login</a></li>
                <li><a href="/register">Register</a></li>
            @endif
            @unless (Auth::guest()) 
                <li><a href="{{ URL::to(auth()->user()->username) }}"><i class="icon-user"></i>Profile</a></li>
                <li class="{{ set_active('transaction/deposit') }}" ><a href="{{ URL::to('transaction/deposit') }}">Deposit</a></li>
                <li class="{{ set_active('transaction/withdraw') }}" ><a href="{{ URL::to('transaction/withdraw') }}">Withdraw</a></li>
                <li class="{{ set_active('transaction/history') }}" ><a href="{{ URL::to('transaction/history') }}">History</a></li>
                <li class="{{ set_active(['game/account/', 'game/account/*']) }}" ><a href="{{ URL::to('game/account/' . auth()->user()->id) }}">Game Account</a></li>
                <li><a href="{{ URL::to('auth/logout') }}"><i class="icon-remove"></i>Log out</a></li>
            @endif
        </ul>
        <a href="#" id="openup">Menu</a>
    </div>
    @unless (Auth::guest()) 
        <div class="sub-nav">
            <ul class="inner-sub-nav">
                <li class="{{ set_active('transaction/deposit') }}" ><a href="{{ URL::to('transaction/deposit') }}">Deposit</a></li>
                <li class="{{ set_active('transaction/withdraw') }}" ><a href="{{ URL::to('transaction/withdraw') }}">Withdraw</a></li>
                <li class="{{ set_active('transaction/history') }}" ><a href="{{ URL::to('transaction/history') }}">History</a></li>
                <li class="{{ set_active(['game/account/', 'game/account/*']) }}" ><a href="{{ URL::to('game/account/' . auth()->user()->id) }}">Game Account</a></li>
                {{-- <li class="{{ set_active('transaction/transfer') }}" ><a href="{{ URL::to('transaction/transfer') }}">Transfer</a></li> --}}
            </ul>
        </div>
    @endif
</nav>