  @if (Session::has('flash_message'))
    <section class="flash-region success">
        <p>{!! Session::get('flash_message') !!}</p>
    </section>
  @endif