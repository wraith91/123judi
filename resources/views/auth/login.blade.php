@extends('layouts.app.master')

@section('title')
    Login
@stop

@section('contents')
<div class="deposit-content">
    <div class="deposit-space w100"></div><!-- deposit-space -->
    <div class="deposit-wrapper container">
        <div class="breadcrumb">
            <a href="index.html">
                Home
            </a>
            &nbsp;
            <span>
                &gt;
            </span>
            &nbsp;
            Login Member
        </div><!-- breadcrumb -->
        <div class="custom-sb w100">
            <div class="login-single positionRelative">
                @include ('errors.list')
                <div class="login-register-title-single taCenter">
                    login member
                </div><!-- login-register-title -->
                <div class="login-register-content">
                    {!! Form::open(['route' => 'auth.postLogin']) !!}
                        <div class="login-register-form-item-container w100">
                            {!! Form::text('username', null, ['class' => 'login-register-form-item w100', 'placeholder' => 'Username', 'tabindex' => '1']) !!}
                        </div><!-- login-register-form-item-container -->
                        <div class="login-register-form-item-container w100">
                            {!! Form::password('password', ['class' => 'login-register-form-item w100', 'placeholder' => 'Password', 'tabindex' => '2']) !!}
                        </div><!-- login-register-form-item-container -->
                        <div class="login-register-form-item-container-noborder w100">
                            <label>
                                <input type="checkbox" name="login-register-sk" />
                                Ingat saya
                            </label>
                            <span class="floatRight">
                                <a class="login-register-btn" href="reset.html">
                                    Lupa password?
                                </a>
                            </span>
                        </div><!-- login-register-form-item-container -->
                        <div class="w100">
                            {!! Form::submit('LOGIN SEKARANG', ['class' => 'login-register-submit w100 cursorPointer']) !!}
                        </div><!-- login-register-form-item-container -->
                        <div class="login-register-form-item-container-noborder taCenter w100">
                            Belum punya akun?&nbsp;
                            <a class="login-register-btn" href="/register">
                                Daftar sekarang
                            </a>
                        </div><!-- login-register-form-item-container -->
                    {!! Form::close() !!}
                </div><!-- login-register-content -->
            </div><!-- login-register -->
        </div><!-- custom-sb -->
    </div><!-- deposit-wrapper -->
</div><!-- deposit-content -->
@stop