<!DOCTYPE html>
<html>
    <head>
        <title>Be right back.</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 300;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 30px;
            }

            .body {
                font-size: 24px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <img class="img-logo-agenwin" src="{{ asset('main/app/img/logo/logo-agenwin.png') }}" width="217" height="79" />
                <div class="title">Will be right back.</div>
                <div class="body">We are performing scheduled maintaince. We should be back online shortly.</div>
            </div>
        </div>
    </body>
</html>
