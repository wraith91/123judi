@extends('layouts.app.master')

@section('contents')
<div class="news-page-content">
    <div class="news-page-space w100"></div><!-- news-page-space -->
    <div class="news-page-wrapper container">
        <div class="breadcrumb">
            <a href="/">
                Home
            </a>
            &nbsp;
            <span>
                &gt;
            </span>
            &nbsp;
            <a href="/news">
                News
            </a>
            &nbsp;
            <span class="breadcrumbNewsTitle">
                <span>
                    &gt;
                </span>
                &nbsp;
                {{ $article->title }}
            </span><!-- breadcrumbNewsTitle -->
        </div><!-- breadcrumb -->
        <div class="custom-sb w100">
            <div class="news-detail-wrapper">
                <div class="news-detail-image positionRelative">
                    <img src="{{ asset('http://'. env('IP') .'/' . $article->image) }}" />
                    <div class="news-date positionAbsolute">
                        <div class="news-date-number taCenter">
                            {{ $article->created_at->day }}
                        </div><!-- news-date-number -->
                        <div class="news-date-text taCenter">
                            {{ date("F", mktime(0, 0, 0, $article->created_at->month, 10)) }}
                        </div><!-- news-date-text -->
                    </div><!-- news-date -->
                </div><!-- news-detail-image -->
                <div class="news-detail-text">
                    <p class="news-detail-text-title taCenter">
                        {{ $article->title }}
                    </p>
                    
                    {!! $article->body !!}
                    
                </div><!-- news-detail-text -->
                <div class="news-detail-nav positionRelative">
                    <div class="news-detail-nav-left positionAbsolute">
                        <a href="#">
                            &lt; previous
                        </a>
                    </div>
                    <div class="news-detail-nav-right positionAbsolute">
                        <a href="#">
                            next &gt;
                        </a>
                    </div>
                </div><!-- news-detail-nav -->
                <div class="news-page-space-mobile"></div><!-- news-page-space-mobile -->
            </div><!-- news-detail-wrapper -->
        </div><!-- custom-sb -->
    </div><!-- news-page-wrapper -->
</div><!-- news-page-content -->
@stop
