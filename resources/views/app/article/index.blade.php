@extends('layouts.app.master')

@section('contents')
<div class="news-page-content">
    <div class="news-page-space w100"></div><!-- news-page-space -->
    <div class="news-page-wrapper container">
        <div class="breadcrumb">
            <a href="index.html">
                Home
            </a>
            &nbsp;
            <span>
                &gt;
            </span>
            &nbsp;
            News
        </div><!-- breadcrumb -->
        <div class="news-page-box-wrapper">
            @foreach ($articles as $article)
                <div class="news-page-news-layer crsl-item">
                    <a href="{{ URL::to('/news/' . strtolower($article->title))}}">
                        <div class="news-left-content floatLeft">
                            <div class="news-image positionRelative">
                                <img src="{{ asset('http://'. env('IP') .'/' . $article->image) }}" />
                                <div class="news-date positionAbsolute">
                                    <div class="news-date-number taCenter">
                                        {{ $article->created_at->day }}
                                    </div><!-- news-date-number -->
                                    <div class="news-date-text taCenter">
                                        {{ date("F", mktime(0, 0, 0, $article->created_at->month, 10)) }}
                                    </div><!-- news-date-text -->
                                </div><!-- news-date -->
                            </div><!-- news-image -->
                        </div><!-- news-left-content -->
                        <div class="news-right-content floatLeft">
                            <div class="news-title">
                                {{ str_limit($article->title, 40) }}
                            </div><!-- news-image -->
                            <div class="news-content">
                                {!! str_limit($article->body, 150 ) !!}
                            </div><!-- news-image -->
                        </div><!-- news-left-content -->
                    </a>
                </div><!-- news-page-news-layer -->
            @endforeach
            <div class="clearBoth"></div><!-- clearBoth -->
        </div><!-- news-page-box-wrapper -->

        @include('partials.pagination', ['paginator' => $articles])

        <div class="news-page-space-mobile"></div><!-- news-page-space-mobile -->
    </div><!-- news-page-wrapper -->
</div><!-- news-page-content -->
@stop
