@extends('layouts.app.master')

@section('contents')
	<div class="promo-content">
        <div class="promo-space w100"></div><!-- promo-space -->
        <div class="promo-wrapper container">
            <div class="breadcrumb">
                <a href="index.html">
                    Home
                </a>
                &nbsp;
                <span>
                    &gt;
                </span>
                &nbsp;
                Promo
            </div><!-- breadcrumb -->
            <div class="custom-sb w100">
                <div class="promo-title w100">
                    Promo
                </div><!-- promo-title -->
                <div class="promo-box-wrapper">
                    <div id="promo-box-1" class="promo-box cursorPointer">
                        <img src="{{ asset('main/app/img/promo/image-promo-123Judi-1.png') }}">
                    </div><!-- promo-box-1 -->
                    <div id="promo-box-text-1" class="promo-box-text">
                        <ul>
                            <li>Bonus rollingan live casino sebesar 1.2%* akan kami berikan kepada para pemain dalam keadaan menang ataupun kalah pada minggu tersebut.</li>
                            <li>Rollingan/ komisi 1.2%* untuk live casino (Sbobet/Calibet/IBCbet casino) akan ditambah secara manual ke account anda setiap hari Senin jam 14.00 siang WIB.</li>
                            <li>Hanya berlaku untuk hitungan permainan Live Casino (Calibet/IBCbet Casino / SBObet Casino ) saja</li>
                        </ul> <br>

                       <h3 style="margin-bottom: 5px;">VIP Member</h3>
                       <ul>
                           <li>Komisi 1,2 % .turnover mencapai 50x dari total deposit selama seminggu</li>
                           <li>Komisi 1,1 % .turnover mencapai 35x dari total deposit selama seminggu</li>
                           <li>Komisi 1 %   .turnover mencapai 20 x dari total deposit selama seminggu</li>
                        </ul><br>
                        <h3 style="margin-bottom: 5px;">Normal Member</h3>
                        <ul>
                           <li>Komisi 0,7% ,tanpa syarat</li>
                       </ul>
                    </div><!-- promo-box-text-1 -->
                    <div id="promo-box-7" class="promo-box cursorPointer">
                        <img src="{{ asset('main/app/img/promo/image-promo-123Judi-7.png') }}">
                    </div><!-- promo-box-7 -->
                    <div id="promo-box-text-7" class="promo-box-text">
                        <p>
                           30 % BONUS UNTUK SPORTSBOOK (SBOBET, IBCBET)
                        </p>
                        <p>
                            Bonus 30% diberikan untuk deposit awal minimal 1juta.<br><br>
                            Bonus maksimal di berikan Rp 3juta
                            Maksimal Bet per Match adalah disesuaikan dengan nilai deposit.<br><br>
                            Harus capai Turnover 20x dari total nilai deposit. ex : deposit 1.000.000, maka turnover harus capai 20 x (1.000.000+30%) = 26.000.000
                            Draw Bet &amp Reject Bet tidak termasuk Turn Over.<br><br>
                            Tidak diperkenankan melakukan Bet kiri kanan dalam 1 partai yang sama.<br><br>
                            Bonus berlaku untuk seluruh jenis taruhan Sportbooks kecuali Casino.<br><br>
                            Bonus tidak berlaku bila ditemukan IP address yang sama dengan member lainnya.<br><br>
                            Kami berhak membatalkan Bonus apabila ditemukan kecurangan, No Risk Bet, Abnormal Bett (Manipulasi Bonus).
                        </p>
                    </div><!-- promo-box-text-7 -->
                    <div id="promo-box-8" class="promo-box cursorPointer">
                        <img src="{{ asset('main/app/img/promo/image-promo-123Judi-8.png') }}">
                    </div><!-- promo-box-8 -->
                    <div id="promo-box-text-8" class="promo-box-text">
                        <p>
                            *Jika Skor 0-0 Selama 90 Menit pasangan anda kami refund. <br>
                            HOT Promo "No Goal"
                        </p>
                        <p>
                            * Apabila pertandingan tidak ada goal (Score 0:0) dan anda kalah maka uang pasangan anda akan kami kembalikan.
                        </p>
                        <p>
                            Syarat dan ketentuan : <br>
                            <ul>
                                <li>Segera konfirmasi kepada CS kami untuk mengikuti promo ini. </li>
                                <li>Untuk maximal bonus senilai Rp 1.000.000/partai.</li>
                                <li>Bonus hanya berlaku untuk handicap FT dan bola mati saja. Jika ada pasangan lain pada partai tersebut, tidak akan mendapatkan refund.</li>
                                <li>Refund hanya sejumlah nominal pasangan, persentase kekalahan kei tidak dapat di refund.</li>
                                <li>Bonus hanya bisa di klaim 1x / minggu.</li>
                                <li>Refund hanya untuk pertandingan liga besar di Divisi Utama. Liga yang termasuk adalah:
                                    <ul>
                                       <li>* Inggris, Jerman, Spanyol, Italia, Perancis, Belanda.</li>
                                       <li>* Liga antar klub eropa Champions dan UEFA berlaku mulai fase grup.</li>
                                       <li>* Worldcup, EURO cup dan Copa America hanya event utama saja tidak termasuk kualifikasi.</li>
                                       <li>
                                           <ul>
                                               <li>&nbsp&nbsp&nbsp- Untuk pasaran 1/4 refund 1/4 dari total pasangan. Maximal pasang Rp 4.000.000,- Maximal refund Rp 1.000.000,- </li>
                                               <li>&nbsp&nbsp&nbsp- Untuk pasaran 1/2 refund 1/2 dari total pasangan. Maximal pasang Rp 2.000.000,- Maximal refund Rp 1.000.000,-</li>
                                               <li>&nbsp&nbsp&nbsp- Untuk pasaran 3/4 refund 3/4 dari total pasangan.</li>
                                               <li>&nbsp&nbsp&nbsp- Untuk pasaran 1 dan 1 keatas refund full.</li>
                                           </ul>
                                       </li>
                                   </ul>
                                </li>
                                <li>Promosi ini tidak dapat digabungkan dengan promo lainnya.</li>
                            </ul>
                        </p>
                    </div><!-- promo-box-text-8 -->
                    <div id="promo-box-2" class="promo-box cursorPointer">
                        <img src="{{ asset('main/app/img/promo/image-promo-123Judi-2.png') }}">
                    </div><!-- promo-box-2 -->
                    <div id="promo-box-text-2" class="promo-box-text">
                        <p>
                            Promo Mix Parlay untuk master betting mix parlay
                        </p>
                        <p>
                            Bonus berlaku untuk semua member
                        </p>
                        <p>
                            Promosi ini hanya berlaku untuk taruhan mix parlay
                        </p>
                        <p>
                            Jika semua partai yang anda mainkan menang semua dalam 1 paket mix parlay,maka kami akan memberikan saldo lagi untuk kemenangan member
                        </p>
                        <p>
                            Contoh : mix parlay yang menang full (tidak ada draw dan kalah 1/4)  minimal oddsnya 30.00 <br>
                                      pasang 100 akan kita berikan bonus tambahan sebesar 100 * 3 ( sesuai odds yang di menangkan 30 - 39 ) = 300 <br>
                                                 100 * 4 ( sesuai odds yang di menangkan 40 - 49 ) = 400 <br>
                                                100 * 5 ( sesuai odds yang di menangkan 50 - max) = 500 <br>
                                                Promo bonus ini diberikan setiap hari senin dan member bisa klaim pribadi melalui livechat 123judi.
                        </p>
                        <p>
                            ======= <br> <br>

                            <strong>PERHATIAN :</strong> <br><br>
                            Jika pihak management 123judi mendapatkan bukti atau indikasi kecurangan penyalah-gunaan promo ini (penipuan, manipulasi, bonus hunter, dsb).
                             123judi  tidak akan mengembalikan dana deposit & membatalkan dana kemenangan member yang bermasalah tersebut.
                             serta 123judi akan menyebarkan nama dan no rekening member yang bermasalah tersebut ke agen lainnya.
                            123judi mengadakan bonus ini sebagai bentuk hiburan & kenyamanan untuk member 123judi.
                        </p>
                    </div><!-- promo-box-text-2 -->
                    <div id="promo-box-3" class="promo-box cursorPointer">
                        <img src="{{ asset('main/app/img/promo/image-promo-123Judi-3.png') }}">
                    </div><!-- promo-box-3 -->
                    <div id="promo-box-text-3" class="promo-box-text">
                        <p>
                            Bonus akan di berikan setelah menyelesaikan TO 4X dari nilai deposit<br><br>
                        </p>
                        <p>
                            Bonus batal apabila melakukan taruhan Mix parlay,1x2,Correct Score (no risk bet).
                        </p>
                        <p>
                            Minimal Deposit <br>
                              Rp200.000 <br> <br>
                            Maximal Bonus <br>
                              Rp2.000.000
                        </p>
                    </div><!-- promo-box-text-3 -->
                    <div id="promo-box-4" class="promo-box cursorPointer">
                        <img src="{{ asset('main/app/img/promo/image-promo-123Judi-4.png') }}">
                    </div><!-- promo-box-4 -->
                    <div id="promo-box-text-4" class="promo-box-text">
                        <p>
                            Nikmati Pasif income dengan merekomendasikan teman anda bergabung dengan kami.
                        </p>
                        <p>
                            Hanya berlaku untuk sporstbook (sbobet &amp ibcbet) – dapatkan bonus berbentuk commision 0.25% dari total turnover teman anda secara cuma2 (acc teman anda tetap akan mendapatkan commision normal 0.25%, ini hanya bonus tambahan untuk yg mereferensikan).
                        </p>
                        <p>
                            Sewaktu mendaftar anda / teman anda (salah satu) harus menkonfirmasi dengan cs kami anda telah merekomendasikan teman (batas waktu 1x 24jam) Bonus 0.25% commision turnover teman anda akan diberikan ke account anda setiap hari senin. Dihitung periode senin – minggu sebelumnya.
                        </p>
                        <p>
                            Tidak ada batas untuk merekomendasikan teman. Semakin bnyk anda merekomendasikan, semakin bnyk turnover & commision yg anda dapatkan !
                        </p>
                    </div><!-- promo-box-text-4 -->
                    <div id="promo-box-5" class="promo-box cursorPointer">
                        <img src="{{ asset('main/app/img/promo/image-promo-123Judi-5.png') }}">
                    </div><!-- promo-box-5 -->
                    <div id="promo-box-text-5" class="promo-box-text">
                        <p>
                            Bonus cashback sebesar 3% akan kami berikan kepada pemain dalam keadaan minus (kalah) pada minggu tersebut.
                        </p>
                        <p>
                            (Dihitung dari total kekalahan pemain setiap periode Senin – Minggu sebelumnya)
                            Cashback 3% untuk casino akan ditambah oleh staff kami setiap hari Senin.
                        </p>
                        <p>
                            - Minimum Cashback Rp. 30.000,- (kalah 1 juta) &amp Maksimum Cashback Rp. 900.000,- (kalah 30jt) <br>
                            Cashback 3% akan diberikan dengan minimal turnover : total kekalahan x 30 <br>
                            Contoh: Jika anda kalah 5000, berarti turnover anda harus 5000 x 30 = 150.000 untuk mendapatkan cashback 3% <br>
                            Hanya berlaku untuk permainan Casino (Calibet/sbobet casino/ibcbet casino ) saja
                        </p>
                    </div><!-- promo-box-text-5 -->
                    <div id="promo-box-6" class="promo-box cursorPointer">
                        <img src="{{ asset('main/app/img/promo/image-promo-123Judi-6.png') }}">
                    </div><!-- promo-box-6 -->
                    <div id="promo-box-text-6" class="promo-box-text">
                        <p>
                           Bonus Cashback sebesar 5% akan kami berikan kepada para pemain dalam keadaan minus (kalah) pada minggu tersebut.(Dihitung dari total kekalahan pemain setiap periode Senin-Minggu sebelumnya)
                        </p>
                        <p>
                            Bonus akan diberikan setiap hari Senin Sore
                        </p>
                        <p>
                            Minimum Cashback Rp 50.000,- (kalah 1jt) <br><br>
                            Tidak ada maximum Cashback (kalah 1 milyar dapet Cashback 50jt)<br><br>
                            Minimum Turnover 5x dari total kekalahan. (Jika kalah 5jt maka turnover harus mencapai 25jt).<br><br>
                            Jika turnover tidak mencukupi target (dibawah 5x kekalahan ) anda tetap akan mendapatkan Cashback sebesar 3% tanpa batas maksimal (contoh : Kalah 1M akan mendapat Cashback 30jt jika Turnover tidak mencukupi )?<br><br>
                            Hanya berlaku untuk hitungan permainan Sportsbook (SBObet / IBCbet ) saja
                        </p>
                    </div><!-- promo-box-text-6 -->
                </div><!-- promo-box-wrapper -->
            </div><!-- custom-sb -->
        </div><!-- promo-wrapper -->
    </div><!-- promo-content -->
@stop

@section('footer')
    <script type="text/javascript" src="{{ asset('main/app/js/promo.js') }}"></script>
@stop
