@extends('layouts.app')

@section('header')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@stop

@section('contents')
	<div class="subpage-titles fadeIn animated">
	  <div class="inner-content">
	      <h2 class="subpage-title">{{ Request::path() }}</h2>
	  </div>
	</div>
    
    @include ('errors.list')

    @include ('partials.flash')

    <section class="content-region">
        <div class="inner-left-region-sm wow fadeInLeft animated" data-wow-delay="0.5s">
            <h4 class="section-header">Kontak yang tersedia</h4>
            <div class="social-center">
                <div class="social-content">
                    <img src="{{ asset('main/images/sms.png') }}" alt="sms">
                    <span>SMS</span>
                </div>
                <div class="social-id">
                    <a href="tel:+081295735777"><b>0812-9573-5777</b></a>
                </div>
            </div>
            <div class="social-center">
                <div class="social-content">
                    <img src="{{ asset('main/images/whatsapp.png') }}" alt="whatsapp">
                    <span>Whatsapp</span>
                </div>
                <div class="social-id">
                    <b>+63-905-539-7579</b>
                </div>
            </div>
            <div class="social-center">
                <div class="social-content">
                    <img src="{{ asset('main/images/bbm.png') }}" alt="bbm">
                    <span>BBM</span>
                </div>
                <div class="social-id">
                    <b>58dd2aff</b>
                </div>
            </div>
        </div>
        {!! Form::open(['url' => 'contact']) !!}
            <div class="inner-right-region-lg wow fadeInRight animated" data-wow-delay="0.5s">
                <h4 class="section-header">Administrator akan membalas pesan Anda melalui email aktif</h4>
                @if (auth()->guest())
                    <div class="field-wrapper-block lg">
                          {!! Form::label('name', 'Nama: *') !!}
                          {!! Form::text('name', null, ['class' => 'form-input']) !!}
                    </div>
                    <div class="field-wrapper-block lg">
                          {!! Form::label('email', 'Email: *') !!}
                          {!! Form::text('email', null, ['class' => 'form-input']) !!}
                    </div>
                @else 
                    {!! Form::hidden('name', auth()->user()->profile->first_name) !!}
                    {!! Form::hidden('email', auth()->user()->email) !!}
                @endif
                <div class="field-wrapper-block lg">
                      {!! Form::label('inquiry_type_id', 'Inquiry Type:') !!}
                      {!! Form::select('inquiry_type_id', $type, null, ['class' => 'form-select']) !!}
                </div>
                <div class="field-wrapper-block lg">
                      {!! Form::label('subject', 'Subyek: *') !!}
                      {!! Form::text('subject', null, ['class' => 'form-input']) !!}
                </div>
                <div class="field-wrapper-block lg">
                      {!! Form::label('body', 'Pesan ke Administrator untuk keluhan & Support: *') !!}
                      {!! Form::textarea('body', null, ['class' => 'form-textarea']) !!}
                </div>
                <span class="reminder">Dengan mengirimkan formulir ini, Anda :</span>
                <ol class="reminder-list">
                    <li>Pastikan Anda berusia minimal 18 tahun</li>
                    <li>Pastikan akun 9betGroup anda aktif.</li>
                    <li>Setuju untuk menerima taruhan gratis dan penawaran menarik dari 9betGroup, dan memahami bahwa Anda dapat mengubah preferensi komunikasi Anda setiap saat di "Detail saya" bagian "Account saya".</li>
                </ol>
                <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
                <div class="field-wrapper-inline">
                    {!! Form::submit('Submit', ['class' => 'form-button active']) !!}
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@stop

