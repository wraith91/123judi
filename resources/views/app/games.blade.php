@extends('layouts.app.master')

@section('contents')
	<div class="casino-content">
        <div class="casino-space w100"></div><!-- casino-space -->
        <div class="casino-wrapper container">
            <div class="breadcrumb">
                <a href="/">
                    Home
                </a>
                &nbsp;
                <span>
                    &gt;
                </span>
                &nbsp;
                Games
            </div><!-- breadcrumb -->
            <div class="custom-sb w100">
                <div class="casino-box-wrapper">
                    <div class="casino-box floatLeft">
                        <a href="{{ URL::to('game-sabung') }}">
                            <img src="{{ asset('main/app/img/logo/logo-sabungAyam-123Judi.png') }}" width="180" height="180" />
                        </a>
                    </div><!-- casino-box -->

                    <div class="clearBoth"></div><!-- clearBoth -->
                </div><!-- casino-box-wrapper -->
            </div><!-- custom-sb -->
        </div><!-- casino-wrapper -->
    </div><!-- casino-content -->
@stop