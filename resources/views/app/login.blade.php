<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="AgenWin official template design"/>
		<meta name="keywords" content="agenwin,template,responsive,design"/>
		<meta name="author" content="Ivan Solid Circle">
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Oswald">
		<link rel="stylesheet" type="text/css" media="screen and (min-width: 800px)" href="{{ asset('main/app/css/desktop.css') }}" />
		<link rel="stylesheet" type="text/css" media="screen and (max-width: 700px)" href="{{ asset('main/app/css/mobile.css') }}" />
		<link rel="stylesheet" type="text/css" media="screen and (min-width: 701px) and (max-width: 799px)" href="{{ asset('main/app/css/tablet.css') }}" />
		<script type="text/javascript" src="{{ asset('main/app/js/jquery.1.12.0.min.js') }}"></script>
	</head>
	<body>
		<div class="login positionRelative">
			<div class="login-register-close cursorPointer taCenter positionAbsolute">
				X
			</div><!-- login-register-close -->
			<div class="login-register-title taCenter">
				login member
			</div><!-- login-register-title -->
			<div class="login-register-content">
				{!! Form::open(['url' => 'auth/login']) !!}
					<div class="login-register-form-item-container w100">
						{!! Form::text('username', null, ['class' => 'login-register-form-item w100', 'placeholder' => 'Username', 'tabindex' => '1']) !!}
					</div><!-- login-register-form-item-container -->
					<div class="login-register-form-item-container w100">
						{!! Form::password('password', ['class' => 'login-register-form-item w100', 'placeholder' => 'Password', 'tabindex' => '2']) !!}
					</div><!-- login-register-form-item-container -->
					<div class="login-register-form-item-container-noborder w100">
	<!-- 					<label>
							<input type="checkbox" name="login-register-sk" />
							Ingat saya
						</label>
						<span class="floatRight">
							<a class="login-register-btn" href="reset.html">
								Lupa password?
							</a>
						</span> -->
					</div><!-- login-register-form-item-container -->
					<div class="w100">
						{!! Form::submit('LOGIN SEKARANG', ['class' => 'login-register-submit w100 cursorPointer']) !!}
					</div><!-- login-register-form-item-container -->
					<div class="login-register-form-item-container-noborder taCenter w100">
						Belum punya akun?&nbsp;
						<a class="login-register-btn" href="/register">
							Daftar sekarang
						</a>
					</div><!-- login-register-form-item-container -->
				{!! Form::close() !!}  
			</div><!-- login-register-content -->
		</div><!-- login-register -->
		<script type="text/javascript" src="{{ asset('main/app/js/login-register.js') }}"></script>
	</body>
</html>