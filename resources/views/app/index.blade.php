@extends('layouts.app.master')

@section('contents')
	<div class="main-content w100">
		<div class="main-content-top container">
			<div class="slider-wrapper">
				<ul class="slides">
{{-- 					<li>
						<div class="slide-01 slide-content positionRelative">
							<img class="positionAbsolute girl-01" src="main/app/img/slider/slide-6-player.png" />
							<img class="positionAbsolute box-01" src="main/app/img/slider/slide-4-box.png" />
						</div><!-- slide-01 -->
					</li> --}}
					<li>
						<div class="slide-01 slide-content positionRelative">
							<img class="positionAbsolute girl-01" src="main/app/img/slider/slide-1-player.png" />
							<img class="positionAbsolute box-01" src="main/app/img/slider/slide-2-box.png" />
						</div><!-- slide-01 -->
					</li>
					<li>
						<div class="slide-02 slide-content positionRelative">
							<img class="positionAbsolute girl-02" src="main/app/img/slider/slide-2-player.png" />
							<img class="positionAbsolute box-02" src="main/app/img/slider/slide-1-box.png" />
						</div><!-- slide-02 -->
					</li>
					<li>
						<div class="slide-01 slide-content positionRelative">
							<img class="positionAbsolute girl-01" src="main/app/img/slider/slide-3-player.png" />
							<img class="positionAbsolute box-01" src="main/app/img/slider/slide-3-box.png" />
						</div><!-- slide-01 -->
					</li>
				</ul><!-- slides -->
			</div><!-- slider-wrapper -->
			<div class="custom-navigation positionRelative">
				<a href="#" class="flex-prev positionAbsolute"></a>
				<div class="custom-controls-container positionRelative"></div>
				<a href="#" class="flex-next positionAbsolute"></a>
			</div>
		</div><!-- main-content-top -->
		<div class="main-content-mid-wrapper container">
			<div class="main-content-mid">
				<div class="main-content-mid-title floatLeft">
					<div class="main-content-mid-title-1 taCenter">
						daftar &amp; bermain bersama kami
					</div><!-- main-content-mid-title-1 -->
					<div class="main-content-mid-title-2 taCenter">
						hanya dengan 3 Langkah mudah
					</div><!-- main-content-mid-title-2 -->
				</div><!-- main-content-mid-title -->
				<div class="main-content-mid-daftar main-content-mid-box floatLeft">
					<a class="cursorPointer taCenter" href="/register">
						<img class="icon-mid-content" src="main/app/img/icon/icon-daftar.png" /><br />
						daftar
					</a>
				</div><!-- main-content-mid-daftar -->
				<div class="main-content-mid-deposit main-content-mid-box floatLeft">
					<a class="cursorPointer taCenter" href="/deposit">
						<img class="icon-mid-content" src="main/app/img/icon/icon-deposit.png" /><br />
						deposit
					</a>
				</div><!-- main-content-mid-deposit -->
				<div class="main-content-mid-play main-content-mid-box floatLeft">
					<a class="cursorPointer taCenter" href="/auth/login">
						<img class="icon-mid-content" src="main/app/img/icon/icon-play.png" /><br />
						play
					</a>
				</div><!-- main-content-mid-play -->
			</div><!-- main-content-mid -->
		</div><!-- main-content-mid-wrapper -->
		<div class="main-content-btm container">
			<div class="our-games-title taCenter">
				our games
			</div><!-- our-games-title -->
			<div class="our-games-content">
				<img src="main/app/img/logo/logo-sbobet.png" />
				<img src="main/app/img/logo/logo-ibcbet.png" />
				<img src="main/app/img/logo/logo-calibet.png" />
				<img src="main/app/img/logo/logo-klikpoker.png" />
				<img src="main/app/img/logo/logo-ibcbet.png" />
				<img src="main/app/img/logo/logo-sbobet.png" />
				<img src="main/app/img/logo/logo-klikpoker.png" />
				<img src="main/app/img/logo/logo-calibet.png" />
			</div><!-- our-games-content -->
		</div><!-- main-content-btm -->
	</div><!-- main-content -->
	<div class="main-news w100">
		<div class="container">
			<div class="main-news-title w100 taCenter">
				berita bola
			</div><!-- main-news-title -->
			<div id="nav-01" class="crsl-nav positionRelative">
				<a href="#" class="previous positionAbsolute"></a>
				<a href="#" class="next positionAbsolute"></a>
			</div><!-- nav-01 -->
			<div class="crsl-wrapper">
				<div class="crsl-items" data-navigation="nav-01">
					<div class="main-news-wrapper crsl-wrap">
						@foreach ($articles as $article)
							<div class="main-news-layer floatLeft crsl-item">
								<a href="{{ URL::to('/news/' . strtolower($article->title))}}">
									<div class="news-left-content floatLeft">
										<div class="news-image positionRelative">
                                			<img src="{{ asset('http://'. env('IP') .'/' . $article->image) }}" />
											<div class="news-date positionAbsolute">
												<div class="news-date-number taCenter">
													{{ $article->created_at->day }}
												</div><!-- news-date-number -->
												<div class="news-date-text taCenter">
													{{ date("F", mktime(0, 0, 0, $article->created_at->month, 10)) }}
												</div><!-- news-date-text -->
											</div><!-- news-date -->
										</div><!-- news-image -->
									</div><!-- news-left-content -->
									<div class="news-right-content floatLeft">
										<div class="news-title">
											{{ str_limit($article->title, 40) }}
										</div><!-- news-image -->
										<div class="news-content">
											{!! str_limit($article->body, 150 ) !!}
										</div><!-- news-image -->
									</div><!-- news-left-content -->
								</a>
							</div><!-- main-news-layer -->
						@endforeach
					</div><!-- main-news-wrapper -->
				</div><!-- crsl-items -->
			</div><!-- crsl-wrapper -->
			<div class="main-news-title w100 taCenter">
				<a class="btn-view-all-news" href="/news">
					semua berita
				</a>
			</div><!-- main-news-title -->
		</div><!-- container -->
	</div><!-- main-news -->
@stop