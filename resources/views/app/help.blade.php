@extends('layouts.app.master')

@section('contents')
	<div class="help-content">
        <div class="help-space w100"></div><!-- deposit-space -->
        <div class="help-wrapper container">
            <div class="breadcrumb">
                <a href="/">
                    Home
                </a>
                &nbsp;
                <span>
                    &gt;
                </span>
                &nbsp;
                Help
            </div><!-- breadcrumb -->
            <div class="custom-sb w100">
                <div class="help-detail-title w100">
                    Help
                </div><!-- help-detail-title -->
                <div class="help-detail-text">
                    <div class="help-item-wrapper w100">
                        <div id="help-item-title-1" class="help-item-title cursorPointer">
                            <div class="help-item-arrow box_transition taCenter floatLeft">
                                &gt;
                            </div><!-- help-item-arrow -->
                            <div class="help-item-title-text">
                                Bagaimana untuk bergabung dan bermain di 9betgroup?
                            </div><!-- help-item-title-text -->
                        </div><!-- help-item-title -->
                        <div id="help-item-content-1" class="help-item-content">
                            Pilih daftar menu dan mengisi formulir sesuai dengan data asli dan lengkap. <a href="#">klik disini</a>.
                        </div><!-- help-item-content -->
                    </div><!-- help-item-wrapper -->
                    <div class="help-item-wrapper w100">
                        <div id="help-item-title-2" class="help-item-title cursorPointer">
                            <div class="help-item-arrow box_transition taCenter floatLeft">
                                &gt;
                            </div><!-- help-item-arrow -->
                            <div class="help-item-title-text">
                                Bagaimana saya menyetor dana ke 9betgroup akun?
                            </div><!-- help-item-title-text -->
                        </div><!-- help-item-title -->
                        <div id="help-item-content-2" class="help-item-content">
                            <p>Sebelum taruhan melalui 9betGroup, Anda akan perlu untuk mendanai account Anda menggunakan salah satu pilihan deposit berikut:</p> <br>
                            <ol>
                                <li>Bank internet</li>
                                <li>Mobile Banking</li>
                                <li>SMS Banking</li>
                                <li>Phone Banking</li>
                                <li>ATM</li>
                            </ol>
                            <br>
                            <b>Catatan:</b>
                            <ul>
                                <li>Silahkan bertanya akun 9betGroup sebelum membuat deposit. Setelah menyetorkan dana deposito pilih menu dan mengisi formulir dengan deposit lengkap atau hubungi layanan pelanggan kami melalui kontak yang tersedia.</li>
                                <li>Silahkan Menyimpan Bukti transaksi Anda sampai Anda telah selesai memproses transaksi.</li>
                                <li>Kami menerima semua pembayaran melalui bank lokal seperti BCA, Mandiri, BNI, BRI .</li>
                            </ul>
                        </div><!-- help-item-content -->
                    </div><!-- help-item-wrapper -->
                    <div class="help-item-wrapper w100">
                        <div id="help-item-title-3" class="help-item-title cursorPointer">
                            <div class="help-item-arrow box_transition taCenter floatLeft">
                                &gt;
                            </div><!-- help-item-arrow -->
                            <div class="help-item-title-text">
                                Bagaimana saya menarik dana dari 9betgroup akun?
                            </div><!-- help-item-title-text -->
                        </div><!-- help-item-title -->
                        <div id="help-item-content-3" class="help-item-content">
                           Pilih menu ditarik setelah isi formulir sesuai dengan data asli dan lengkap. Penarikan hanya dapat ditransfer ke rekening yang terdaftar di akun 9betGroup.
                        </div><!-- help-item-content -->
                    </div><!-- help-item-wrapper -->
                    <div class="help-item-wrapper w100">
                        <div id="help-item-title-4" class="help-item-title cursorPointer">
                            <div class="help-item-arrow box_transition taCenter floatLeft">
                                &gt;
                            </div><!-- help-item-arrow -->
                            <div class="help-item-title-text">
                                Berapa lama proses untuk deposit dan menarik dana dari 9betgroup?
                            </div><!-- help-item-title-text -->
                        </div><!-- help-item-title -->
                        <div id="help-item-content-4" class="help-item-content">
                           <table>
                            <thead>
                                <td>Metode</td>
                                <td>Pengolahan Waktu</td>
                                <td>Dana yang diterima oleh Anda (Perkiraan)</td>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Deposit (Deposit Dana)</td>
                                    <td>15 menit</td>
                                    <td>Langsung ke rekening</td>
                                </tr>
                                <tr>
                                    <td>Penarikan (Tarik Dana)</td>
                                    <td>15 menit</td>
                                    <td>Maksimum 1 jam </td>
                                </tr>
                            </tbody>
                        </table><br>
                        <b>Note:</b>
                        <ul>
                            <li>We accept all payments through the local bank such as BCA, Mandiri, BNI, BRI, CIMB Niaga, BANK GEM, and all the other local banks.</li>
                            <li>For Members who do Withdraw and deposit at the time of the transaction Offline Bank will be processed immediately after the Bank Online.</li>
                        </ul>
                        </div><!-- help-item-content -->
                    </div><!-- help-item-wrapper -->
                    <div class="help-item-wrapper w100">
                        <div id="help-item-title-5" class="help-item-title cursorPointer">
                            <div class="help-item-arrow box_transition taCenter floatLeft">
                                &gt;
                            </div><!-- help-item-arrow -->
                            <div class="help-item-title-text">
                                Adalah layanan pelanggan di 9betgroup 24 Jam?
                            </div><!-- help-item-title-text -->
                        </div><!-- help-item-title -->
                        <div id="help-item-content-5" class="help-item-content">
                            Layanan pelanggan kami selalu online 24 jam. <br/>
                            Kami siap untuk melayani semua pelanggan kami dengan profesional dan ramah.<br/>
                            Jika ada keluhan terhadap layanan kami silahkan hubungi kami langsung oleh admin email seniormaster cs@9bet.com.
                        </div><!-- help-item-content -->
                    </div><!-- help-item-wrapper -->
                    <div class="help-item-wrapper w100">
                        <div id="help-item-title-6" class="help-item-title cursorPointer">
                            <div class="help-item-arrow box_transition taCenter floatLeft">
                                &gt;
                            </div><!-- help-item-arrow -->
                            <div class="help-item-title-text">
                                Cara mengikuti bonus yang ada di 9betgroup?
                            </div><!-- help-item-title-text -->
                        </div><!-- help-item-title -->
                        <div id="help-item-content-6" class="help-item-content">
                           Untuk mengikuti Bonus - Bonus yang ditawarkan oleh 9betGroup Anda dapat melihat persyaratan dalam Promosi menu dan langsung menghubungi Customer Service kami untuk bergabung promo.
                        </div><!-- help-item-content -->
                    </div><!-- help-item-wrapper -->
                    <div class="help-item-wrapper w100">
                        <div id="help-item-title-7" class="help-item-title cursorPointer">
                            <div class="help-item-arrow box_transition taCenter floatLeft">
                                &gt;
                            </div><!-- help-item-arrow -->
                            <div class="help-item-title-text">
                                Cara mendapatkan ID dari permainan?
                            </div><!-- help-item-title-text -->
                        </div><!-- help-item-title -->
                        <div id="help-item-content-7" class="help-item-content">
                           Untuk mendapatkan account permainan, Anda harus mendaftar terlebih dahulu dan kemudian Anda mengkonfirmasi ke Layanan Pelanggan kami untuk melakukan deposit terlebih dahulu setelah itu Anda akan mendapatkan pertandingan ID / akun yang Anda inginkan.
                        </div><!-- help-item-content -->
                    </div><!-- help-item-wrapper -->
                    <div class="help-item-wrapper w100">
                        <div id="help-item-title-8" class="help-item-title cursorPointer">
                            <div class="help-item-arrow box_transition taCenter floatLeft">
                                &gt;
                            </div><!-- help-item-arrow -->
                            <div class="help-item-title-text">
                                Aku digunakan untuk memiliki di game ID 9betgroup tapi sekarang tidak bisa login?
                            </div><!-- help-item-title-text -->
                        </div><!-- help-item-title -->
                        <div id="help-item-content-8" class="help-item-content">
                           Bagi Anda yang belum pernah menggunakan permainan ID 9betGroup Anda dan tidak bisa login, maka Anda dapat mengisi formulir pendaftaran kembali atau Anda dapat mengkonfirmasi dengan Customer Service kami untuk memperbarui data Anda setelah Anda melakukan deposit maka Anda langsung mendapatkan ID baru dari permainan Anda.
                        </div><!-- help-item-content -->
                    </div><!-- help-item-wrapper -->
                </div><!-- help-detail-text -->
                <div class="help-detail-title w100">
                    Peraturan
                </div>
                <div class="help-detail-text">
                    <div class="help-item-wrapper w100">
                        <div id="help-item-title-9" class="help-item-title cursorPointer">
                            <div class="help-item-arrow box_transition taCenter floatLeft">
                                &gt;
                            </div><!-- help-item-arrow -->
                            <div class="help-item-title-text">
                                Dengan mendaftar sebagai 123judi atau Member taruhan online di 123judi...
                            </div><!-- help-item-title-text -->
                        </div><!-- help-item-title -->
                        <div id="help-item-content-9" class="help-item-content">
                            Dengan mendaftar sebagai 123judi atau Member taruhan online di 123judi,  berarti anda telah mengerti dan menyetujui sepenuhnya peraturan-peraturan yang ada di 123judi <br><br>

                            Usia minimum membuka account adalah 18 tahun.Member bertanggung jawab penuh terhadap ID &amp Password yang didapatkan melalui 123judi Member diwajibkan membaca, mengerti, dan dianggap menyetujui peraturan-peraturanyang ditetapkan oleh perusahaan judi online pihak ketiga seperti SBObet, IBCbet,DLL, apabila sudah terdaftar sebagai member 123judi Data yang anda berikan ke kami adalah akurat, karena data ini tidak dapat diubah, terutama no rekening bank dengan alasan apapun, dan kami akan melakukan pengiriman Penarikan / Withdrawal dana ke rekening bank yang anda berikan disaatpertama kali mendaftar.Jam Operasional Customer Service kami adalah 24 jam setiap harinya.Jam untuk DEPOSIT ataupun WITHDRAW dapat dilakukan setiap saat 24/7,  kecuali dijam offline bank. Permintaan Deposit &amp Withdraw akan langsung di proses langsungsetelah menerima FORM / Konfirmasi format dari media LIVE CHAT, BBM, SMS, WHATSAPP, WECHAT, LINE dari member bersangkutan.JUMLAH minimal TRANSAKSI Rp. 200.000Total Withdraw dan Transfer antar account maksimal 4x per hari.Untuk transfer credit antar account minimal 1x turnover dari deposit awal, tidakdiperkenankan untuk deposit di account A, lalu tanpa dimainkan dan dipindahkan ke account B, C dst. 123judi berhak untuk menolak / menutup  member baru / lama jika member tersebut bermasalah, atau tidak dapat mengikuti peraturan-peraturan 123judi
                        </div><!-- help-item-content -->
                    </div><!-- help-item-wrapper -->
                </div>
            </div><!-- custom-sb -->
        </div><!-- help-wrapper -->
    </div><!-- help-content -->
@stop

@section('footer')
    <script type="text/javascript" src="{{ asset('main/app/js/help.js') }}"></script>
@stop
 