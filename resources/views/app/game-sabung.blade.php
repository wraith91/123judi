@extends('layouts.app.master')

@section('contents')
    <div class="casino-content">
        <div class="casino-space w100"></div><!-- casino-space -->
        <div class="casino-wrapper container">
            <div class="breadcrumb">
                <a href="/">
                    Home
                </a>
                &nbsp;
                <span>
                    &gt;
                </span>
                &nbsp;
                <a href="casino">
                    Game
                </a>
                &nbsp;
                <span>
                    &gt;
                </span>
                &nbsp;
                Sabung Ayam
            </div><!-- breadcrumb -->
            <div class="custom-sb w100">
                <div class="casino-detail-title w100">
                    Sabung Ayam - <strong>S128</strong>
                </div><!-- casino-detail-title -->
                <div class="casino-detail-text">
                    <img src="{{ asset('main/app/img/game/s128.jpg') }}" style="margin-bottom: 30px;" />
                    <p>
                        <img src="{{ asset('main/app/img/game/s128-aboutus.png') }}" style="float: left; margin-right: 10px;"/> Dalam setiap organisasi penting bahwa kita berbagi kepercayaan kepada pelanggan. Seperti pelanggan kami yang bijaksana layak untuk tahu bahwa kami bangga dengan produk dan pelayanan kami, memberi kepercayaan merupakan visi dan misi kami.
                    </p>
                    <br>
                    <p style="clear: left;">
                        <span>Visi dari entitas yang relevan</span><br />
                        <ul>
                            <li>S128 berdedikasi dan berkomitmen untuk menyediakan integritas tingkat tertinggi. Layanan dan pengalaman bermain untuk memuaskan pelanggan kami saat ini maupun masa depan pelanggan kami.</li>
                            <li>Federasi Nasional dari Permainan Peternak Unggas, Inc adalah Negara terkemuka atas organisasi permainan peternak unggas yang berdedikasi dan berkomitmen untuk mempromosikan olahraga sebagai sebuah industri dan tradisi yang mulia, yang mana sabung dianggap sebagai hobi nasional dan olahraga yang menggambarkan arti sebenarnya dari kejujuran, integritas dan permainan yang adil.</li>
                            <li>PSSC berusaha untuk mnjadi pemimpin pasar dalam Siaran Langsung Sabung dan penyiaran Filipina bersemnagat untuk memberikan kualitas, dilisensikan dan dapat diandalkan.</li>
                        </ul>
                    </p>
                    <p>
                        <span>Misi dari entitas yang relevan</span><br />
                        <ul>
                            <li>Untuk mengangkat industri permainan unggas sebagai salah satu penggerak ekonomi kami.</li>
                            <li>Untuk mempromosikan olahraga sebagai sumber mata pencaharian untuk jutaan masyarakat Fillipina.</li>
                            <li>Untuk melindungi hak yang melekat dan semangat dari setiap pecinta sabung dan menikmati olahraga sebagai bagian dari tradisi Negara kita.</li>
                        </ul>
                    </p>
                </div><!-- casino-detail-text -->
                <div class="btn-register-now-2 taCenter">
                    <a href="/register">
                        daftar sekarang
                    </a>
                </div><!-- btn-register-now-2 -->
            </div><!-- custom-sb -->
        </div><!-- casino-wrapper -->
    </div><!-- casino-content -->
@stop
