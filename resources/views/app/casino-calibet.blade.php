@extends('layouts.app.master')

@section('contents')
	<div class="casino-content">
        <div class="casino-space w100"></div><!-- casino-space -->
        <div class="casino-wrapper container">
            <div class="breadcrumb">
                <a href="/">
                    Home
                </a>
                &nbsp;
                <span>
                    &gt;
                </span>
                &nbsp;
                <a href="casino">
                    Casino
                </a>
                &nbsp;
                <span>
                    &gt;
                </span>
                &nbsp;
                CALIBET
            </div><!-- breadcrumb -->
            <div class="custom-sb w100">
                <div class="casino-detail-title w100">
                    CALIBET
                </div><!-- casino-detail-title -->
                <div class="casino-detail-text">
                    <p>
                        CALIBET merupakan Bandar dan Agen penyedia permainan casino online LIVE dengan jumalah permainan judi online sangat lengkap seperti table games, street games, video poker, dan soft games, slots, card games (permainan kartu) dan lain lain.
                    </p>
                    <p>
                        CaliBET menyediakan meja VIP khusus sehingga Anda akan memiliki andrenalin sebagai player tangguh casino secra LIVE. Jika anda bermain di meja VIP, anda dapat melakukan permintaan seperti dealer bagikan kartu tanpa harus menunggu waktu yang ditentukan. Andapun bisa melakukan permintaan ganti dealer dan ganti kartu jika anda merasa kurang cocok dengan dealer atau kartu di dalam permainan tersebut.
                    </p>
                    <p>
                        123judi merupakan Bandar sekaligus Agen resmi pasar judi online CALIBET di Indonesia, segera DAFTAR menjadi member dan dapatkan pelayanan terbaik dari kami.
                    </p>
                    <p>
                        Atau hubungi kami melalui fasilitas LIVE CHAT, admin kami akan senang membantu Anda.
                    </p>
                    <p>
                        Situs Resmi : <a href="www.calibet.com">www.calibet.com</a>
                    </p>
                </div><!-- casino-detail-text -->
                <div class="casino-detail-image positionRelative taCenter">
                    <img src="{{ asset('main/app/img/casino/def-calibet.png') }}" />
                    <div class="btn-register-now positionAbsolute taCenter">
                        <a href="#">
                            daftar sekarang
                        </a>
                    </div><!-- btn-register-now -->
                </div><!-- casino-detail-image -->
            </div><!-- custom-sb -->
        </div><!-- casino-wrapper -->
    </div><!-- casino-content -->
@stop
