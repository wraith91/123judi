@extends('layouts.app.master')

@section('contents')
	<div class="casino-content">
        <div class="casino-space w100"></div><!-- casino-space -->
        <div class="casino-wrapper container">
            <div class="breadcrumb">
                <a href="/">
                    Home
                </a>
                &nbsp;
                <span>
                    &gt;
                </span>
                &nbsp;
                <a href="casino">
                    Casino
                </a>
                &nbsp;
                <span>
                    &gt;
                </span>
                &nbsp;
                SBOBET Casino
            </div><!-- breadcrumb -->
            <div class="custom-sb w100">
                <div class="casino-detail-title w100">
                    SBOBET Casino
                </div><!-- casino-detail-title -->
                <div class="casino-detail-text">
                    <p>
                        SBOBet Casino, yang dulu lebih dikenal dengan nama 338A, merupakan vendor permainan kasino ternama yang menyediakan banyak pilihan game casino online.
                    </p>
                    <p>
                        Dengan menggunakan sistem Multi Player Live Dealer yang dikelola bersama vendor online gaming di Australia menjadikan SBOBet Casino sebagai salah satu pilihan utama para pecinta casino online.
                    </p>
                    <p>
                        123judi sebagai agen SBOBet Casino menyediakan berbagai variasi permainan, seperti: Poker, Baccarat, Roulette, Keno, dan masih banyak lagi. Anda akan dibuat betah dengan kombinasi permainan tak terbatas.
                    </p>
                    <p>
                        123judi merupakan Super Master Agen SBOBet Casino, bersamaan dengan Super Master Agen untuk SBOBet Sportsbook, yang tentunya akan berusaha yang terbaik untuk memberikan kepuasan pada para member setia dengan layanan dan proses deposit serta withdraw yang cepat.
                    </p>
                    <p>
                        Bergabunglah di 123judi selaku agen SBOBet Casino terpercaya di Indonesia dan rasakan asyiknya bermain game casino online.
                    </p>
                </div><!-- casino-detail-text -->
                <div class="casino-detail-image positionRelative taCenter">
                    <img src="{{ asset('main/app/img/casino/def-sbobetcasino.png') }}" />
                    <div class="btn-register-now positionAbsolute taCenter">
                        <a href="#">
                            daftar sekarang
                        </a>
                    </div><!-- btn-register-now -->
                </div><!-- casino-detail-image -->
            </div><!-- custom-sb -->
        </div><!-- casino-wrapper -->
    </div><!-- casino-content -->
@stop
