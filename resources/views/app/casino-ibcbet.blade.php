@extends('layouts.app.master')

@section('contents')
	<div class="casino-content">
        <div class="casino-space w100"></div><!-- casino-space -->
        <div class="casino-wrapper container">
            <div class="breadcrumb">
                <a href="/">
                    Home
                </a>
                &nbsp;
                <span>
                    &gt;
                </span>
                &nbsp;
                <a href="casino">
                    Casino
                </a>
                &nbsp;
                <span>
                    &gt;
                </span>
                &nbsp;
                IBCBET Casino
            </div><!-- breadcrumb -->
            <div class="custom-sb w100">
                <div class="casino-detail-title w100">
                    IBCBET Casino
                </div><!-- casino-detail-title -->
                <div class="casino-detail-text">
                    <p>
                        Produk dan jasa yang ditawarkan melalui website IBCBET dilisensikan dan diatur oleh First Cagayan Leisure dan Resort Corporation (<a href="www.firstcagayan.com">www.firstcagayan.com</a>) untuk Cagayan Economic Zone Authority (<a href="www.ceza.gov.ph">www.ceza.gov.ph</a>) dari pemerintah Filipina.
                    </p>
                    <p>
                        Filosofi IBCBET didasarkan pada membangun merek, posisi, didorong konsep pemasaran online; berbagai produk yang sangat baik dan beragam dengan harga yang kompetitif; layanan pelanggan yang sangat baik, sopan dan dapat diandalkan.
                    </p>
                    <p>
                        Ibcbet adalah salah satu situs judi online terbesar di wilayah asia namun menjangkau skala dunia, Agen Ibcbet dan penggunanya pun tersebar diseluruh banyak negara yang menawarkan berbagai permainan judi cabang olahraga dan kasino yang sangat menarik dan juga lengkap. Keunggulan Ibcbet bisa dilihat dari..
                    </p>
                    <p>
                        <span>Terbaik dalam menjaga Kemanan Data Privasi</span><br />
                        Ibcbet menggunakan sistem enkripsi 128bit yang dikembangkan oleh National Institute of Standards and Technology untuk menjaga keamanan data para penggunanya. Enkirpsi 128bit merupakan enkripsi data level tertinggi saat ini dan nyaris mustahil untuk dipecahkan. Sehingga para petaruh tidak perlu khawatir data pribadi mereka dicuri atau bocor ke tangan pihak lain yang tidak bertanggung jawab.
                    </p>
                    <p>
                        <span>Terbaik dalam pelayanan</span><br />
                        Customer Care IBCBET Tersedia 24 jam sehari, 7 hari, dan dapat dihubungi melalui berbagai metode yang mudah. IBCBET ingin memastikan bahwa pelayanan yang sebenarnya akan selalu tersedia untuk menjawab komentar dan pertanyaan dalam cara yang efisien dan sopan. Prioritas IBCBET adalah memastikan tingkat tertinggi dalam pelayanan pelanggan.
                    </p>
                    <p>
                        <span>Aman dari Penipuan dan Kolusi</span><br />
                        Pemain hanya diperbolehkan memiliki satu Akun per pemain dan IBCBET melakukan pemeriksaan keamanan acak pada sistem untuk menjaga integritas dan keadilan. Jika IBCBET menemukan pemain yang berpartisipasi dalam kolusi atau praktek menipu lainnya, IBCBET tidak segan untuk menutup account mereka dengan segera.
                    </p>
                    <p>
                        <span>Dapat dipertanggungjawabkan</span><br />
                        Perusahaan IBCBET berkomitmen untuk bertanggung jawab penuh terhadap segala kegiatan perjudian. Perusahaan IBCBET juga memiliki peraturan dan pedoman seperti yang disediakan oleh otoritas perjudian jarak jauh dan terus menjadi operator perjudian jarak jauh yang bertanggung jawab secara sosial.
                    </p>
                </div><!-- casino-detail-text -->
                <div class="btn-register-now-2 taCenter">
                    <a href="#">
                        daftar sekarang
                    </a>
                </div><!-- btn-register-now-2 -->
            </div><!-- custom-sb -->
        </div><!-- casino-wrapper -->
    </div><!-- casino-content -->
@stop
