@extends('layouts.app.master')

@section('contents')
<div class="deposit-content">
    <div class="deposit-space w100"></div><!-- deposit-space -->
    <div class="deposit-wrapper container">
        <div class="breadcrumb">
            <a href="/">
                Home
            </a>
            &nbsp;
            <span>
                &gt;
            </span>
            &nbsp;
            Daftar Member
        </div><!-- breadcrumb -->
        <div class="custom-sb w100">
            <div class="register-single positionRelative">
                @include ('errors.list')
                <div class="login-register-title-single taCenter">
                    daftar member
                </div><!-- login-register-title -->
                <div class="login-register-content">
                    {!! Form::open(['route' => 'registration.store']) !!}
                        <div class="register-form-wrapper w100">
                            <div class="register-form-left register-form-div floatLeft">
                                <div class="login-register-form-item-container w100">
                                    {!! Form::text('username', null, ['class' => 'login-register-form-item w100', 'required' => 'required', 'placeholder' => 'Nama Punguna *']) !!}
                                </div><!-- login-register-form-item-container -->
                                <div class="login-register-form-item-container w100">
                                    {!! Form::text('email', null, ['class' => 'login-register-form-item w100', 'required' => 'required', 'placeholder' => 'Email *']) !!}
                                </div><!-- login-register-form-item-container -->
                                <div class="login-register-form-item-container w100">
                                    {!! Form::password('password', ['class' => 'login-register-form-item w100', 'required' => 'required', 'placeholder' => 'Password *']) !!}
                                </div><!-- login-register-form-item-container -->
                                <div class="login-register-form-item-container w100">
                                    {!! Form::password('confirm_password', ['class' => 'login-register-form-item w100', 'required' => 'required', 'placeholder' => 'Konfirmasi Password *']) !!}
                                </div><!-- login-register-form-item-container -->
                            </div><!-- register-form-left -->
                            <div class="register-form-right register-form-div floatLeft">
                                <div class="login-register-form-item-container w100">
                                    {!! Form::text('first_name', null, ['class' => 'login-register-form-item w100', 'placeholder' => 'Nama']) !!}
                                </div><!-- login-register-form-item-container -->
                                <div class="login-register-form-item-container w100">
                                    {!! Form::text('mobile', null, ['class' => 'login-register-form-item w100', 'placeholder' => 'No HP *']) !!}
                                </div><!-- login-register-form-item-container -->
       <!--                          <div class="login-register-form-item-container w100">
                                    {!! Form::input('date', 'birth_date', null, ['id' => 'datepicker' ,'class' => 'login-register-form-item w100']) !!}
                                </div> -->
                                <!-- login-register-form-item-container -->
                                <div class="login-register-form-item-container w100">
                                    {!! Form::select('bank_type_id', $bank_name, null, ['class' => 'login-register-form-item w100']) !!}
                                </div><!-- login-register-form-item-container -->
                                <div class="login-register-form-item-container w100">
                                    {!! Form::text('account_name', null, ['class' => 'login-register-form-item w100', 'required' => 'required', 'placeholder' => 'Nama Rekening *']) !!}
                                </div><!-- login-register-form-item-container -->
                                <div class="login-register-form-item-container w100">
                                    {!! Form::text('account_no', null, ['class' => 'login-register-form-item w100', 'required' => 'required', 'placeholder' => 'Nomor Rekening *']) !!}
                                </div>
                            </div><!-- register-form-right -->
                            <div class="clearBoth"></div><!-- clearBoth -->
                        </div><!-- register-form-wrapper -->
                        <div class="login-register-form-item-container-noborder w100">
                            <!--<label>
                                <input type="checkbox" name="login-register-sk" />
                                Saya telah menyetujui <a href="syarat.html" target="_blank">syarat &amp; ketentuan</a>
                            </label>-->
                            By submitting this form, you:<br />
                            &nbsp;<br />
                            <ul class="register-ul">
                                <li class="register-li">
                                    Confirm that you are at least 18 years old.
                                </li>
                                <li class="register-li">
                                    Confirm that you do not hold an existing, active 123judi account.
                                </li>
                                <li class="register-li">
                                    Agree to receive free bets and exciting offers from 123judi, and understand that you can change your communication preferences at any time in the "My Details" section of "My Account".
                                </li>
                            </ul>
                        </div><!-- login-register-form-item-container -->
                        <div class="register-submit">
                            {!! Form::submit('DAFTAR SEKARANG', ['class' => 'login-register-submit w100 cursorPointer']) !!}
                        </div><!-- login-register-form-item-container -->
                        <div class="login-register-form-item-container-noborder taCenter w100">
                            Sudah punya akun?&nbsp;
                            <a class="login-register-btn" href="login.html">
                                Login di sini
                            </a>
                        </div><!-- login-register-form-item-container -->
                    {!! Form::close() !!}
                </div><!-- login-register-content -->
            </div><!-- login-register -->
        </div><!-- custom-sb -->
    </div><!-- deposit-wrapper -->
</div><!-- deposit-content -->
@stop
