@extends('layouts.app.master')

@section('contents')
<div class="deposit-content">
    <div class="deposit-space w100"></div><!-- deposit-space -->
    <div class="deposit-wrapper container">
        <div class="breadcrumb">
            <a href="index.html">
                Home
            </a>
            &nbsp;
            <span>
                &gt;
            </span>
            &nbsp;
            Deposit
        </div><!-- breadcrumb -->
        <div class="custom-sb w100">
          @include ('errors.list')

          @include('partials.flash')

          @include('partials.info')
            <div class="deposit-detail-title w100">
                Deposit
            </div><!-- deposit-detail-title -->
            <div class="deposit-detail-text">
                <p>
                    Mohon diisi dengan benar untuk mempercepat transaksi anda dengan kami.
                </p>
                <p>
                    <div class="deposit-form">
                        @if (!in_array_r('Pending', $user->transaction->toArray()))
                        {!! Form::model($user->bankAccount, ['url' => 'transaction']) !!}
                            {!! Form::hidden('trxn_type_id', '1') !!}
                            <div class="deposit-form-component">
                                <div class="deposit-form-component-left floatLeft">
                                    {!! Form::label('account_name', 'Account name') !!}
                                </div><!-- deposit-form-component-left -->
                                <div class="deposit-form-component-right floatLeft">
                                    {!! Form::text('account_name', null, ['class' => 'text-user-id deposit-form-component-stdsize', 'required' => 'required', 'readonly']) !!}
                                </div><!-- deposit-form-component-right -->
                            </div><!-- deposit-form-component -->                           
                            <div class="deposit-form-component">
                                <div class="deposit-form-component-left floatLeft">
                                    {!! Form::label('account_no', 'Account number') !!}
                                </div><!-- deposit-form-component-left -->
                                <div class="deposit-form-component-right floatLeft">
                                    {!! Form::text('account_no', null, ['class' => 'text-user-id deposit-form-component-stdsize', 'required' => 'required', 'readonly']) !!}
                                </div><!-- deposit-form-component-right -->
                            </div><!-- deposit-form-component -->
                            <div class="deposit-form-component">
                                <div class="deposit-form-component-left floatLeft">
                                    {!! Form::label('game_id', 'Select Game *') !!}
                                </div><!-- deposit-form-component-left -->
                                <div class="deposit-form-component-right floatLeft">
                                    {!! Form::select('game_id', $game, null, ['class' => 'select-permainan deposit-form-component-stdsize']) !!}
                                </div><!-- deposit-form-component-right -->
                            </div><!-- deposit-form-component -->                           
                            <div class="deposit-form-component">
                                <div class="deposit-form-component-left floatLeft">
                                    {!! Form::label('amount', 'Amount *') !!}
                                </div><!-- deposit-form-component-left -->
                                <div class="deposit-form-component-right floatLeft">
                                    {!! Form::text('amount', null, ['class' => 'text-user-id deposit-form-component-stdsize', 'required' => 'required']) !!}
                                </div><!-- deposit-form-component-right -->
                            </div><!-- deposit-form-component -->
                            <div class="deposit-form-component">
                                <div class="deposit-form-component-left floatLeft"></div><!-- deposit-form-component-left -->
                                <div class="deposit-form-component-right floatLeft">
                                    {!! Form::submit('Submit', ['class' => 'submit-form taCenter cursorPointer deposit-form-component-submit']) !!}
                                </div><!-- deposit-form-component-right -->
                            </div><!-- deposit-form-component -->
                        {!! Form::close() !!}
                        @endif
                    </div><!-- deposit-form -->
                </p>
                <p>
                    Selain menggunakan Formulir ini, Konfirmasi Deposit juga dapat dilakukan dengan cara menghubungi customer service bertugas kami melalui Live Chat atau Yahoo Messenger (<a href="ymsgr:sendim?agenwin">agenwin@yahoo.com</a>) dan juga melalui SMS ke nomor +6282279123123 dan juga dari BBM (Pin 2B00B5EB) dengan format SMS :
                </p>
                <p>
                    <small>
                        Ketentuan deposit MANDIRI yang transfer melalui mesin ATM, tidak boleh menggunakan MESIN SETORAN TUNAI &amp; JENIS NON CASH/ NON TUNAI.<br />
                        Ketentuan deposit BNI yang transfer melalui mesin ATM, member WAJIB/HARUS menggunakan MESIN ATM BNI tidak boleh menggunakan MESIN SETORAN TUNAI / MESIN ATM Bersama / MESIN ATM bank lain.<br />
                        Ketentuan deposit BRI yang transfer melalui mesin ATM, member WAJIB/HARUS menggunakan MESIN ATM BRI tidak boleh menggunakan MESIN ATM Bersama / MESIN ATM bank lain.
                    <small>
                </p>
            </div><!-- deposit-detail-text -->
        </div><!-- custom-sb -->
    </div><!-- deposit-wrapper -->
</div><!-- deposit-content -->
@stop