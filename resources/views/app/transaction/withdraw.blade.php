@extends('layouts.app.master')

@section('contents')
<div class="deposit-content">
  <div class="deposit-space w100"></div><!-- deposit-space -->
  <div class="deposit-wrapper container">
    <div class="breadcrumb">
      <a href="index.html">
        Home
      </a>
      &nbsp;
      <span>
        &gt;
      </span>
      &nbsp;
      Withdraw
    </div><!-- breadcrumb -->
    @include ('errors.list')

    @include('partials.flash')

    @unless (!in_array_r('Pending', $user->transaction->toArray()) && !in_array_r('To Be Approve', $user->transaction->toArray()) && !in_array_r('Processing', $user->transaction->toArray()))
      <table class="display-table">
        <thead>
            <tr>
                <th>Date of Withdraw</th>
                <th>Requested Amount</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($user->transaction as $transaction)
              @if ($transaction->trxn_status_id != 7 && $transaction->trxn_status_id != 4)
                <tr>
                    <td>{{ $transaction->created_at }}</td>
                    <td>{{ number_format( abs($transaction->amount), 0 , '' , '.' ) }}</td>
                    <td>{{ $transaction->status->name }}</td>
                    <td>
                    @if ($transaction->trxn_status_id != 5 && $transaction->trxn_status_id != 6)
                        {!! Form::open(['method' => 'PATCH', 'url' => ['transaction/withdraw', $transaction->id]]) !!}
                            <div class="field-wrapper-block sm">
                              {!! Form::submit('Cancel Withdraw', ['class' => 'form-button active']) !!}
                            </div>
                        {!! Form::close() !!}
                    @endif
                    </td>
                </tr>
              @endif
            @endforeach
        </tbody>
      </table>
    @endif
    <table class="display-table">
      <thead>
          <tr>
              <th>Date Withdraw</th>
              <th>Withdraw Amount</th>
              <th>Game Account</th>
              <th>Date Completed</th>
              <th>Status</th>
              <th>Reason</th>
          </tr>
      </thead>
      <tbody>
          @foreach ($user->transaction as $transaction)
              <tr>
                  <td>{{ $transaction->created_at }}</td>
                  <td>{{ number_format( abs($transaction->amount), 0 , '' , '.' ) }}</td>
                  <td>{{ $transaction->gameType->name }}</td>
                  <td>{{ $transaction->updated_at }}</td>
                  <td>{{ $transaction->status->name }}</td>
                  <td>
                  @if ($transaction->status->name == 'Rejected')
                      {{ $transaction->remarks }}
                  @endif
                  </td>
              </tr>
          @endforeach
      </tbody>
    </table>
    <div class="custom-sb w100">
      <div class="deposit-detail-title w100">
        Withdraw
      </div><!-- deposit-detail-title -->
      <div class="deposit-detail-text">
        <p>
          Mohon diisi dengan benar untuk mempercepat transaksi anda dengan kami.
        </p>
        <p>
          <div class="deposit-form">
          @if (!in_array_r('Pending', $user->transaction->toArray()) && !in_array_r('To Be Approve', $user->transaction->toArray()) && !in_array_r('Processing', $user->transaction->toArray()))
            @if (!$user->gameAccount->count() < 1)
            {!! Form::open(['url' => 'transaction']) !!}
              {!! Form::hidden('trxn_type_id', '2') !!}
              <div class="deposit-form-component">
                <div class="deposit-form-component-left floatLeft">
                  {!! Form::label('game_id', 'Select Game *') !!}
                </div><!-- deposit-form-component-left -->
                <div class="deposit-form-component-right floatLeft">
                  {!! Form::select('game_id', $type, null, ['class' => 'select-permainan deposit-form-component-stdsize']) !!}
                </div><!-- deposit-form-component-right -->
              </div><!-- deposit-form-component -->
              <div class="deposit-form-component">
                <div class="deposit-form-component-left floatLeft">
                  {!! Form::label('amount', 'Amount *') !!}
                </div><!-- deposit-form-component-left -->
                <div class="deposit-form-component-right floatLeft">
                  {!! Form::text('amount', null, ['class' => 'text-jumlah-deposit deposit-form-component-stdsize', 'required' => 'required']) !!}
                </div><!-- deposit-form-component-right -->
              </div><!-- deposit-form-component -->
              <div class="deposit-form-component">
                <div class="deposit-form-component-left floatLeft"></div><!-- deposit-form-component-left -->
                <div class="deposit-form-component-right floatLeft">
                  {!! Form::submit('Submit', ['class' => 'submit-form taCenter cursorPointer deposit-form-component-submit']) !!}
                </div><!-- deposit-form-component-right -->
              </div><!-- deposit-form-component -->
              @else
              <h4>Its like you don't have a game account, please deposit first.</h4>
              @endif
            {!! Form::close() !!}
          @endif
          </div><!-- deposit-form -->
        </p>
        <p>
          Selain menggunakan Formulir ini, Konfirmasi Deposit juga dapat dilakukan dengan cara menghubungi customer service bertugas kami melalui Live Chat atau Yahoo Messenger (<a href="ymsgr:sendim?agenwin">agenwin@yahoo.com</a>) dan juga melalui SMS ke nomor +6282279123123 dan juga dari BBM (Pin 2B00B5EB) dengan format SMS :
        </p>
        <p>
          (Contoh Format Deposit)<br />
          WD/User ID/Jumlah<br />
          WD/aau123456/100.000
        </p>
      </div><!-- deposit-detail-text -->
    </div><!-- custom-sb -->
  </div><!-- deposit-wrapper -->
</div><!-- deposit-content -->
  	
@stop
