@extends('layouts.app.master')

@section('title')
  {!! $user->profile->first_name . " " . $user->profile->last_name !!}
@stop

@section('contents')
<div class="casino-content">
  <div class="casino-space w100"></div><!-- casino-space -->
  <div class="casino-wrapper container">
    <div class="breadcrumb">
      <a href="index.html">
        Home
      </a>
      &nbsp;
      <span>
        &gt;
      </span>
      &nbsp;
      Games
    </div><!-- breadcrumb -->
    <div class="custom-sb w100">
      @include ('errors.list')

      @include('partials.flash')
      <div class="table-box-wrapper">
      {!! Form::model($user->toArray(), ['method' => 'PUT', 'route' => ['member::profile.update', $user->username]]) !!}
        <div class="input-container">
          {!! Form::label('first_name', 'First Name') !!}
          {!! Form::text('first_name', $user->profile->first_name, ['class' => 'inputs']) !!}
          <br>
          <br>
          {!! Form::label('last_name', 'Last Name') !!}
          {!! Form::text('last_name', $user->profile->last_name, ['class' => 'inputs']) !!}
          <br>
          <br>
          {!! Form::label('gender', 'Gender') !!}
          {!! Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], $user->profile->gender,['class' => 'inputs']) !!}
          <br>
          <br>
          {!! Form::label('mobile', 'Mobile') !!}
          {!! Form::text('mobile', $user->profile->mobile, ['class' => 'inputs']) !!}
          <br>
          <br>
          {!! Form::submit('Update', ['class' => 'inputs blue-btn']) !!}
        </div>
        <div class="clearBoth"></div><!-- clearBoth -->
      {!! Form::close() !!}
      </div><!-- casino-box-wrapper -->
    </div><!-- custom-sb -->
  </div><!-- casino-wrapper -->
</div><!-- casino-content -->
@stop
