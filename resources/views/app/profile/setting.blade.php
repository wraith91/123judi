@extends('layouts.app')

@section('title')
  Profile/Settings
@stop

@section('contents')
  <div class="subpage-titles fadeIn animated">
    <div class="inner-content">
        <h2 class="subpage-title">Settings</h2>
    </div>
  </div>

  @include ('errors.list')

  @include('partials.flash')
  <div class="content-region">
    {!! Form::open(['method' => 'PATCH', 'url' => ['/settings', Auth::User()->id]]) !!}
      <div class="inner-content-region">
        <h4 class="section-header">Password Setting</h4>
        <div class="field-wrapper-block lg">
          {!! Form::label('password', 'Current Password') !!}
          {!! Form::password('old_password', ['class' => 'form-input']) !!}
        </div>
        <div class="field-wrapper-block lg">
          {!! Form::label('password', 'New Password') !!}
          {!! Form::password('password', ['class' => 'form-input']) !!}
        </div>
        <div class="field-wrapper-block lg">
          {!! Form::label('password_confirmation', 'Confirm Password') !!}
          {!! Form::password('password_confirmation', ['class' => 'form-input']) !!}
        </div>
      </div>
      <div class="inner-content-region">
        <div class="field-wrapper-inline">
            {!! Form::submit('Update', ['class' => 'form-button active']) !!}
        </div>
      </div>
      
    {!! Form::close() !!}
  </div>
@stop