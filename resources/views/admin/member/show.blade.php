@extends('layouts.admin.master')

@section('title')
  Members: Information
@stop

@section('content')
  @include('layouts.admin.secondary_header_menu', [
    'title' => 'Member Information', 
    'secondary_menu' => 
      [
        'Back' => 'admin/members/information'
      ]
    ])
  @include('errors.list')
  <div class="block-group">
    <div class="block">
      <div class="block-title"><h4>Game Profile:</h4></div>
      <table class="information-table">
        <thead>
          <tr>
            <th>No.</th>
            <th>From</th>
            <th>To</th>
            <th>Created</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($logs as $l => $log)          
            <tr>
              <td>{{ $l+1 }}</td>
              <td>{{ $log->from }}</td>
              <td>{{ $log->to }}</td>
              <td>{{ $log->created_at }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div> {{-- END OF GAME PROFILE  --}}
  </div>
@stop
