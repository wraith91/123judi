<table class="information-table">
  <thead>
    <tr>
      <th height="20" width="16" >Username</th>
      <th height="20" width="16" >Game Profile ID</th>
      <th height="20" width="16" >Point</th>
      <th height="20" width="16" >Game Type ID</th>
    </tr>
  </thead>
  <tbody id="transactions">
    @foreach ($records as $record)
      <tr>
        <td  height="18">{!! $record->username !!}</td>
        <td  height="18">{!! $record->id !!}</td>
        <td  height="18"></td>
        <td  height="18">{!! $record->game_id !!}</td>
      </tr>
    @endforeach
  </tbody>
</table>