<table class="information-table">
  <thead>
    <tr colspan="11" height="22">
      <th><h1>Deposit Transactions:</h1></th>
    </tr>
    <tr>
      <th height="20" width="16" style="color:blue;">Trxn. ID</th>
      <th height="20" width="16" >System ID</th>
      <th height="20" width="16" >Username</th>
      <th height="20" width="16" >Date</th>
      <th height="20" width="16" >Amount</th>
      <th height="20" width="16" >Trxn Status</th>
      <th height="20" width="16" >From Account no.</th>
      <th height="20" width="16" >From Account name</th>
      <th height="20" width="16" >To Game</th>
      <th height="20" width="16" >To Account</th>
      <th height="20" width="16" >Remarks</th>
      <th height="20" width="16">Modify By</th>

    </tr>
  </thead>
  <tbody id="transactions">
    @foreach ($transactions as $transaction)
      <tr>
        <td  height="18">{!! 'D'.$transaction->created_at->format('ymd').$transaction->id!!}</td>
        <td  height="18">{!! 'IN' . str_pad($transaction->user->id, 5, "0", STR_PAD_LEFT) !!}</td>
        <td  height="18">{!! $transaction->user->username !!}</td>
        <td  height="18">{!! $transaction->created_at !!}</td>
        <td  height="18" align="right">{{{ number_format((float) $transaction->amount, 2) }}}</td>
        <td  height="18">{!! $transaction->status->name !!}</td>
        <td  height="18" align="right">{!! $transaction->bank->account_no !!}</td>
        <td  height="18">{!! $transaction->bank->account_name !!}</td>
        <td  height="18">{!! $transaction->gameType->name !!}</td>
        <td  height="18">{!! $transaction->bank->bankName->name !!}</td>
        <td  height="18">{!! $transaction->remarks !!}</td>
        <td height="18">{!! $transaction->modify_by !!}</td>
      </tr>
    @endforeach
  </tbody>
</table>
