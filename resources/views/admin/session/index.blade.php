@extends('layouts.min-app')

@section('title')
	Login
@stop

@section('contents')

	<section class="content-region">
        <div class="authentication">
        @include('errors.list')
            {!! Form::open(['url' => 'admin/auth/login']) !!}
                <h4 class="section-header">Login</h4>
                <div class="field-wrapper-block lg">
                    {!! Form::label('username', 'Username') !!}
                    {!! Form::text('username', null, ['class' => 'form-select']) !!}
                </div>
                <div class="field-wrapper-block lg">
                    {!! Form::label('password', 'Password') !!}
                    {!! Form::password('password', ['class' => 'form-input']) !!}
                </div>
                <div class="field-wrapper-block sm">
                    {!! Form::input('checkbox', 'remember_me', null, ['class' => 'form-checkbox']) !!}
                    {!! Form::label('remember_me', 'Remember Me') !!}
                </div>
                <div class="field-wrapper-block">
                    {!! Form::submit('Login', ['class' => 'form-button active']) !!}
                    <a class="form-button default" href="{!! URL::to('/') !!}">Back to home</a>
                </div>
            {!! Form::close() !!}
        </div>
	</section>

@stop