@extends('layouts.admin.master')

@section('title')
	Transactions: Bonus
@stop

@section('content')
	@include('layouts.admin.secondary_header_menu', [
		'title' => 'Bonus', 
		'secondary_menu' => 
			[
				'Sbobet' => 'admin/bonus/game/1',
				'Ibcbet' => 'admin/bonus/game/2',
				'Calibet' => 'admin/bonus/game/3',
				'Klikpoker' => 'admin/bonus/game/4'
			]
		])
	@include('errors.list')

	@include('partials.flash')

	<div class="block block-default">
	  {!! Form::open(['url' => 'admin/bonus/upload', 'files' => 'true']) !!}
			<div class="field-wrapper-inline sm">
	    	{!! Form::file('fileToUpload', ['class' => 'form-file']) !!}
			</div>
	    <div class="field-wrapper-inline sm">
	    	{!! Form::submit('Upload', ['class' => 'form-button active']) !!}
	    </div>
	  {!! Form::close() !!}
	</div>

	<div class="block block-default">
    <div class="block-title"><h4>{{ ucfirst(Request::get('game')) }} Logs:</h4></div>
    <table class="information-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>Username</th>
          <th>Game ID</th>
          <th>Game Type</th>
          <th>Bonus Status</th>
          <th>Bonus Point</th>
          <th>Last Balance</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
				@foreach ($records as $index => $record)
					  {!! Form::open(['method' => 'PATCH', 'url' => ['admin/bonus/game', $record->id]]) !!}
  		        <tr>
			        	<td>{{ $index+1 }}</td>
			          <td>{!! $record->gameAccount->user->username !!}</td>
			          <td>{!! $record->gameAccount->username !!}</td>
			          <td>{!! $record->gameType->name !!}</td>
			          <td>{!! $record->status->name !!}</td>
			          <td>{!! $record->bonus_point !!}</td>
			          <td>{!! Form::text('balance', null, ['class' => 'form-input']) !!}</td>
			          <td>{!! Form::submit('Transfer', ['class' => 'form-button active']) !!}</td>
			        </tr>
					  {!! Form::close() !!}
				@endforeach
      </tbody>
    </table>
  </div>
@stop
