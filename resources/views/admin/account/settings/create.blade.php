@extends('layouts.admin.master')

@section('title')
	Account: Settings
@stop

@section('content')
	@include('layouts.admin.secondary_header_menu', [
    'title' => 'Account Settings'
  ])

	@include('errors.list')

	@include ('partials.flash')

	<div class="block-group">
		{!! Form::open(['method' => 'PATCH','url' => ['admin/account/settings', auth()->user()->id]]) !!}
		<div class="block">
    	<div class="block-title"><h4>Account Details:</h4></div>
	  	<div class="field-wrapper-block lg">
	      {!! Form::label('old_password', 'Old Password:') !!}
	      {!! Form::password('old_password', ['class' => 'form-input']) !!}
	  	</div>	  	
	  	<div class="field-wrapper-block lg">
	      {!! Form::label('password', 'Password:') !!}
	      {!! Form::password('password', ['class' => 'form-input']) !!}
	  	</div>	  	
	  	<div class="field-wrapper-block lg">
	      {!! Form::label('confirm_password', 'Confirm Password:') !!}
	      {!! Form::password('confirm_password', ['class' => 'form-input']) !!}
	  	</div>
	  	<div class="field-wrapper-block lg">
	  		{!! Form::submit('Save', ['class' => 'form-button active']) !!}
	  	</div>
		</div>
		{!! Form::close() !!}
  </div>
@stop
