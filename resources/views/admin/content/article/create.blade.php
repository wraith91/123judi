


@extends('layouts.admin.master')

@section('title')
  Content: Articles
@stop

@section('content')
  @include('layouts.admin.secondary_header_menu', [
    'title' => 'Article Lists',
    'secondary_menu' => ['Back' => 'admin/article']
  ])
  @include('errors.list')
  @include('partials.flash')
	<div class="block">
		<div class="block-title"><h4>Create Article:</h4></div>
		{!! Form::open(['route' => 'admin::admin.article.store', 'files' => true]) !!}
		<div class="field-wrapper-block lg">
		  {!! Form::label('title', 'Title') !!}
		  {!! Form::text('title', null, ['class' => 'form-input']) !!}
		</div>
	  	<div class="field-wrapper-block lg">
	      {!! Form::label('body', 'Body') !!}
	      {!! Form::textarea('body', null, ['id' => 'body', 'class' => 'form-textarea']) !!}
	  	</div>
		<div class="field-wrapper-block lg">
		  {!! Form::label('image', 'Image ') !!} <br>
		  {!! Form::file('image') !!}
		</div>
		<div class="field-wrapper-block lg">
			  {!! Form::label('published_at', 'Publish Date') !!}
			  {!! Form::input('date','published_at', date('Y-m-d'), ['class' => 'form-input']) !!}
		</div>
		<div class="field-wrapper-block lg">
			  {!! Form::label('active', 'Status') !!}
			  {!! Form::select('active', [1 => 'Active', 2 => 'Inactive'], null, ['class' => 'form-select']) !!}
		</div>
	  	<div class="field-wrapper-block lg">
	  		{!! Form::submit('Create', ['class' => 'form-button active']) !!}
	  	</div>
	  {!! Form::close() !!}
	</div>
@stop

@section('footer')
	<script src="//cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
	<script>
		CKEDITOR.replace('body');
	</script>
@stop