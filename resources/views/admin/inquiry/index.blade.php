@extends('layouts.admin.master')

@section('title')
	Inquiry
@stop


@section('content')
	@include('layouts.admin.secondary_header_menu', [
    'title' => 'Inquiry'
  ])

	@include('errors.list')

	@include ('partials.flash')
	  <div class="block-group">
    <div class="block col-6">
      <div class="block-title"><h4>Inquiry List:</h4></div>
				<ul class="message-list">
        @foreach ($inquiries as $inquiry)
				<a href="{{ '?id=' . $inquiry->id }}">
					<li>
						<div class="preview">
							<h4>{{ $inquiry->name . ' (' .$inquiry->subject . ')' }} <small>{{ $inquiry->created_at->diffForHumans() }}</small></h4>
							<p>{{ substr($inquiry->body, 0, 50) }}</p>
						</div>
					</li>
				</a>
        @endforeach
				</ul>
    </div> {{-- END OF PROFILE  --}}
    <div class="block col-6">
      @if(Request::get('id') != null && $msg != null)
        @foreach ($msg as $message)
      		<div class="block-title"><h4>{{ $message->subject }}</h4></div>
	          <div class="field-wrapper-block lg">
	          	{{ $message->body }}
	          </div>
	            {!! Form::open(['method' => 'PATCH', 'url' => ['admin/inquiry', $message->id]]) !!}
	            	<div class="field-wrapper-block">
	            		  {!! Form::label('inquiry_status_id', 'Status') !!}
	            		  {!! Form::select('inquiry_status_id', $status, null,  ['class' => 'form-select']) !!}
	            		  {!! Form::submit('Modify', ['class' => 'form-button active']) !!}
	            	</div>
	            {!! Form::close() !!}
    			</div> {{-- END OF MESSAGE CONTENT --}}
        @endforeach
      @endif
  </div>
@stop