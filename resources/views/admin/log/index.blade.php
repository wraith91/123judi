@extends('layouts.admin.master')

@section('title')
	Transactions: Log
@stop

@section('content')
	@include('layouts.admin.secondary_header_menu', ['title' => 'Log'])
	<div class="block block-default">
    <div class="block-title"><h4>Log Records:</h4></div>
    <table class="information-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>Username</th>
          <th>From</th>
          <th>To</th>
          <th>Date Created</th>
        </tr>
      </thead>
      <tbody>
				@foreach ($logs as $log)
	        <tr>
           <td>{{ $i++ }}</td>
           <td>{{ $log->transaction->user->username }}</td>
           <td>{{ $log->from }}</td>
           <td>{{ $log->to }}</td>
	         <td>{{ $log->created_at }}</td>
	        </tr>
				@endforeach
      </tbody>
    </table>
  </div>
@stop
