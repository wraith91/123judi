<aside>
  <div class="logo">
    <h2>9betgroup</h2>
    {{-- <img src="{{ asset('main/images/adminlogo.png') }}" width="140" height="auto" alt="9bet"/> --}}
  </div>
  <nav>
    <div class="menu-block">
      <h5>Main</h5>
      <ul>
        <li class="{{ set_active(['admin/dashboard']) }}">
          <a href="{{ URL::route('admin::dashboard') }}">
            <img class="iconic" data-src="{{ asset('main/images/dashboard-md.svg') }}" />
            Dashboard
          </a>
        </li>
      </ul>
    </div>
    <div class="menu-block">
      <h5>Members</h5>
      <ul>
        <li class="{{ set_active(['admin/members/lists', 'admin/members/*/edit']) }}">
          <a href="{{ URL::to('admin/members/lists') }}">
            <img class="iconic" data-src="{{ asset('main/images/people-md.svg') }}" />
            Lists
          </a>
        </li>
        <li class="{{ set_active(['admin/members/information']) }}">
          <a href="{{ URL::to('admin/members/information') }}">
            <img class="iconic" data-src="{{ asset('main/images/people-md.svg') }}" />
            Information
          </a>
        </li>
        <li class="{{ set_active(['admin/members/game-id-lists', 'admin/members/game-id-lists/*']) }}">
          <a href="{{ URL::to('admin/members/game-id-lists/1') }}">
            <img class="iconic" data-src="{{ asset('main/images/people-md.svg') }}" />
            Game ID Lists
          </a>
        </li>
        {{-- <li class="{{ set_active(['admin/members/report']) }}">
          <a href="{{ URL::to('#') }}">
            <img class="iconic" data-src="{{ asset('main/images/people-md.svg') }}" />
            Report
          </a>
        </li> --}}
      </ul>
    </div>
    <div class="menu-block">
      <h5>Transactions</h5>
      <ul>
        <li class="{{ set_active(['admin/transactions/deposit', 'admin/transactions/deposit/*']) }}">
          <a href="{{ URL::to('admin/transactions/deposit') }}">
            <img class="iconic" data-src="{{ asset('main/images/excerpt-md.svg') }}" />
            Deposit
          </a>
        </li>
        <li class="{{ set_active(['admin/transactions/withdraw', 'admin/transactions/withdraw/*']) }}">
          <a href="{{ URL::to('admin/transactions/withdraw') }}">
            <img class="iconic" data-src="{{ asset('main/images/excerpt-md.svg') }}" />
            Withdraw
          </a>
        </li>
        <li class="{{ set_active(['admin/transactions/log', 'admin/transactions/log/*']) }}">
          <a href="{{ URL::to('admin/transactions/log') }}">
            <img class="iconic" data-src="{{ asset('main/images/excerpt-md.svg') }}" />
            Log
          </a>
        </li>
      </ul>
    </div>
    <div class="menu-block">
      <h5>Bonus</h5>
      <ul>
        <li class="{{ set_active(['admin/bonus/game', 'admin/bonus/game/*']) }}">
          <a href="{{ URL::to('admin/bonus/game/1') }}">
            <img class="iconic" data-src="{{ asset('main/images/excerpt-md.svg') }}" />
            Upload
          </a>
        </li>
        <li class="{{ set_active(['admin/bonus/history', 'admin/bonus/history/*']) }}">
          <a href="{{ URL::to('admin/bonus/history/1') }}">
            <img class="iconic" data-src="{{ asset('main/images/excerpt-md.svg') }}" />
            History
          </a>
        </li>
      </ul>
    </div>
<!--     <div class="menu-block">
      <h5>Inquiries</h5>
      <ul>
        <li class="{{ set_active(['admin/inquiry',]) }}">
          <a href="{{ URL::to('admin/inquiry') }}">
            <img class="iconic" data-src="{{ asset('main/images/envelope-closed-sm.svg') }}" />
            Pending
          </a>
        </li>
      </ul>
    </div> -->
    <div class="menu-block">
      <h5>Content</h5>
      <ul>
        <li class="{{ set_active(['admin/article',]) }}">
          <a href="{{ URL::to('admin/article') }}">
            <img class="iconic" data-src="{{ asset('main/images/copywriting-sm.svg') }}" />
            Article
          </a>
        </li>
      </ul>
    </div>
    @can('access_system_panel')
      <div class="menu-block">
        <h5>System</h5>
        <ul>
          <li class="{{ set_active(['admin/system/user', 'admin/system/user/*']) }}">
            <a href="{{ URL::to('admin/system/user') }}">
              <img class="iconic" data-src="{{ asset('main/images/tools-md.svg') }}" />
              Authorize User
            </a>
          </li>
        </ul>
      </div>
    @endcan
{{--     <div class="menu-block">
      <h5>Account</h5>
      <ul>
        <li class="{{ set_active(['admin/account/settings']) }}">
          <a href="{{ URL::to('admin/account/settings') }}">
            <img class="iconic" data-src="{{ asset('main/images/settings-md.svg') }}" />
            <span>Account Settings</span>
          </a>
        </li>
        <li>
          <a href="{{ URL::to('auth/logout') }}">
            <img class="iconic" data-src="{{ asset('main/images/account-logout-md.svg') }}" />
            <span>Logout</span>
          </a>
        </li>
      </ul>
    </div> --}}
  </nav>
</aside>