<!doctype html>
<html class="no-js" lang="en">
    <head>
        <title> @yield('title', 'Agenwin') </title>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="betting">
        <meta name="author" content="jyerro">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" type="text/css" href="{{ asset('main/css/admin.css') }}">
        @yield('header')

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="wrap">
            @include('layouts.admin.menu')
            <header>
              <div class="block-group">
                  <div class="col-6 greeting">
                      <h3>Hello {{ auth()->user()->username }}!</h3>
                  </div>
                  <div class="col-6 account">
                    <div id="dd" class="wrapper-dropdown-5" tabindex="1">
                        <img class="iconic" data-src="{{ asset('main/images/person-genderless-md.svg') }}" />
                        <ul class="dropdown">
                          <li class="{{ set_active(['admin/account/settings']) }}">
                            <a href="{{ URL::to('admin/account/settings') }}">
                              <img class="iconic" data-src="{{ asset('main/images/settings-md.svg') }}" />
                              <span>Account Settings</span>
                            </a>
                          </li>
                          <li>
                            <a href="{{ URL::to('auth/logout') }}">
                              <img class="iconic" data-src="{{ asset('main/images/account-logout-md.svg') }}" />
                              <span>Logout</span>
                            </a>
                          </li>
                        </ul>
                    </div>
                  </div>
              </div>
            </header>
            <main>
                @yield('content')
            </main>
        </div>  <!--End of Wrap-->

        <script src="{{ asset('main/js/vendor/admin/all.js') }}"></script>
        <!-- Additional Scripts  -->
        @yield('footer')
        <script>
            function DropDown(el) {
                this.dd = el;
                this.initEvents();
            }
            DropDown.prototype = {
                initEvents : function() {
                    var obj = this;

                    obj.dd.on('click', function(event){
                        $(this).toggleClass('active');
                        event.stopPropagation();
                    }); 
                }
            }

            $(function() {

                var dd = new DropDown( $('#dd') );

                $(document).click(function() {
                    // all dropdowns
                    $('.wrapper-dropdown-5').removeClass('active');
                });
            });
        </script>
        <script>
        var iconic = IconicJS({
          autoInjectSelector: 'img.iconic',
          pngFallback: 'assets/png',
          each: function (svg) {
            console.log('Injected an SVG! ' + svg.id);
          },
          autoInjectDone: function (count) {
            console.log('Auto injection of ' + count + ' SVGs complete. We did it.');
          }
        });
        </script>
        {{-- Google Analytics: change UA-XXXXX-X to be your site's ID. --}}
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
</body>
</html>
