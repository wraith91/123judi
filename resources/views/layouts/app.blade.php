<!doctype html>
<html class="no-js" lang="en">
    <head>
        <title> @yield('title', 'Situs Agen Judi Bola | Taruhan Bola Online | Bandar Bola | 9Bet | SBOBET | IBCBET | CALIBET ') </title>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="Situs Agen Judi Bola Dan Taruhan Bola Online. Agen Bola terpercaya dan terbesar, kami siap melayani 24jam"/>
        <meta name="keywords" content="Taruhan Bola, Judi Bola, Agen Bola, Agen Bola Terpercaya, Agen Bola Terbesar, Bandar Bola, Bandar Judi Online, Bandar Bola Online"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" type="text/css" href="{{ asset('main/css/vendor/all.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('main/css/styles.css') }}">
        @yield('header')

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="container">
            <div class="sidebar">
                <h4>Customer Service</h4>
                <table class="sidebar-content">
                    <tbody>
                        <tr>
                            <td><img src="{{ asset('main/images/sms-sm.png') }}" /></td>
                            <td>-</td>
                            <td>SMS <br> 0812-9573-5777</td>
                        </tr>
                        <tr>
                            <td><img src="{{ asset('main/images/whatsapp-sm.png') }}" /></td>
                            <td>-</td>
                            <td>WHATSAPP <br> +63-905-539-7579</td>
                        </tr>
                        <tr>
                            <td><img src="{{ asset('main/images/bbm-sm.png') }}" /></td>
                            <td>-</td>
                            <td>BBM <br> 58DD2AFF</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <header>
                <div class="inner-content">
                    <div class="logo-block">
                        <a href="/"><h1 class="hide-text show-image">9betGroup</h1></a>
                    </div>
                    <div class="login-block">
                        <div class="inner-login-block">
                            @if ($errors->any())
                                <div class="field-wrapper-block sm login">
                                    <small class="error">{{ $errors->first('username') }}</small>
                                </div>
                            @endif
                            @if (Auth::guest())
                                {!! Form::open(['url' => 'auth/login']) !!}
                                    <div class="field-wrapper-inline sm">
                                        {!! Form::text('username', null, ['class' => 'form-input', 'placeholder' => 'Username', 'tabindex' => '1']) !!}
                                        {!! Form::password('password', ['class' => 'form-input', 'placeholder' => 'Password', 'tabindex' => '2']) !!}
                                        {!! Form::submit('Login', ['class' => 'form-button default']) !!}
                                        <a href="register" class="form-button active">Daftar</a>
                                    </div>
                                    <div class="field-wrapper-inline">
                                        <div class="remember-me">
                                            {!! Form::checkbox('remember')!!}
                                            <label for="Remember">Remember Me</label>
                                        </div>
{{--                                         <div class="forgot-pass">
                                            <a href="#">Forgot Password?</a>
                                        </div> --}}
                                    </div>
                                {!! Form::close() !!}         
                            @else
                                <div id="dd" class="wrapper-dropdown-5" tabindex="1">
                                    Hello {{ ucfirst(Auth::user()->profile->first_name) . " " . ucfirst(Auth::user()->profile->last_name) }}!
                                    <ul class="dropdown">
                                        <li><a href="{{ URL::to(auth()->user()->username) }}"><i class="icon-user"></i>Profile</a></li>
                                        <li><a href="{{ URL::to(auth()->user()->username . '/settings') }}"><i class="icon-cog"></i>Settings</a></li>
                                        {{-- <li><a href="#"><i class="icon-cog"></i>Messages</a></li> --}}
                                        <li><a href="{{ URL::to('auth/logout') }}"><i class="icon-remove"></i>Log out</a></li>
                                    </ul>
                                </div>
                            @endif
                        </div>         
                    </div>
                </div>

                @include ('partials.menu')
                <section class="news-bar">
                  <div class="first">
                    <dl id="ticker-1">
                        <dt>Update</dt>
                            <dd>Selamat kepada pemenang promosi anniversary, daftar pemenang bisa dilihat di www.9betrewards.com Jadilah member VIP dan Ratusan juta menanti anda.</dd>
                        <dt>Update</dt>
                            <dd>Selamat kepada pemenang promosi anniversary, daftar pemenang bisa dilihat di www.9betrewards.com Jadilah member VIP dan Ratusan juta menanti anda.</dd>
                    </dl>
                  </div>
                </section>
            </header>
            <main>

                @yield('contents')

            <a href="#0" class="cd-top">Top</a>
            </main>
            <footer>
                <div class="logo-row">
                <div class="powered-by">
                    <h4 class="footer-title wow fadeIn animated" data-wow-delay="0.5s"><span class="light-fnt">Powered</span> By:</h4>
                    <ul class="wow fadeInDown animated" data-wow-delay="1s">
                        <li><a href="#" class="mandiri">Mandiri</a></li>
                        <li><a href="#" class="bca">BCA</a></li>
                        <li><a href="#" class="bni">BNI</a></li>
                        <li><a href="#" class="bri">BRI</a></li>
                        <li><a href="#" class="first">First Cagayan</a></li>
                    </ul>
                </div>
                <div class="social-media">
                    <h4 class="footer-title wow fadeIn animated" data-wow-delay="1.5s"><span class="light-fnt">Social</span> Media:</h4>
                    <ul class="wow fadeInDown animated" data-wow-delay="2s">
                        <li><a href="#" class="facebook">facebook</a></li>
                        <li><a href="#" class="twitter">twitter</a></li>
                    </ul>
                </div>
                </div>
                <div class="copyright fadeInDown animated">
                    Copyright &copy 2015 9betgroup All Right Reserved.
                </div>
            </footer>
        </div>

        <script src="{{ asset('main/js/vendor/all.js') }}"></script>
        <script src="{{ asset('main/js/all.js') }}"></script> <!-- Gem jQuery -->
        <!-- Additional Scripts  -->
        <!-- Start of LiveChat (www.livechatinc.com) code -->
        <script>
        $(function() {
            menu = $('nav ul');

          $('#openup').on('click', function(e) {
            e.preventDefault(); menu.slideToggle();
          });
          
          $(window).resize(function(){
            var w = $(this).width(); if(w > 480 && menu.is(':hidden')) {
              menu.removeAttr('style');
            }
          });
          
          $('nav li').on('click', function(e) {                
            var w = $(window).width(); if(w < 480 ) {
              menu.slideToggle(); 
            }
          });
          $('.open-menu').height($(window).height());
        });
        </script>
        <script>
            function DropDown(el) {
                this.dd = el;
                this.initEvents();
            }
            DropDown.prototype = {
                initEvents : function() {
                    var obj = this;

                    obj.dd.on('click', function(event){
                        $(this).toggleClass('active');
                        event.stopPropagation();
                    }); 
                }
            }

            $(function() {

                var dd = new DropDown( $('#dd') );

                $(document).click(function() {
                    // all dropdowns
                    $('.wrapper-dropdown-5').removeClass('active');
                });
            });
        </script>
       <script type="text/javascript">
            var __lc = {};
            __lc.license = 3052662;

            (function() {
                var lc = document.createElement('script');
                lc.type = 'text/javascript';
                lc.async = true;
                lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(lc, s);
            })();
        </script>
        <script type="text/javascript">
          $(function() {
            var _scroll = {
              delay: 1000,
              easing: 'linear',
              items: 1,
              duration: 0.07,
              timeoutDuration: 0,
              pauseOnHover: 'immediate'
            };
            $('#ticker-1').carouFredSel({
              width: 980,
              align: false,
              items: {
                width: 'variable',
                height: 35,
                visible: 1
              },
              scroll: _scroll
            });

            //  set carousels to be 100% wide
            $('.caroufredsel_wrapper').css('width', '100%');

            //  set a large width on the last DD so the ticker won't show the first item at the end
            // $('#ticker-2 dd:last').width(2000);
          });
        </script>
        <!-- End of LiveChat code -->

        @yield('footer')

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
