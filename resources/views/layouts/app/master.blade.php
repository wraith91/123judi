<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="123judi"/>
		<meta name="keywords" content="123judi,template,responsive,design"/>
		<title>@yield('title', '123judi Promosi casino online | Agen sbobet | Casino online sbobet')</title>
		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('main/app/img/icon/favicon.ico') }}">
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Yanone+Kaffeesatz">
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" >
		<link rel="stylesheet" type="text/css" href="{{ asset('main/app/css/site-features.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('main/app/css/general.css') }}" />
		<link rel="stylesheet" type="text/css" media="screen and (min-width: 800px)" href="{{ asset('main/app/css/desktop.css') }}" />
		<link rel="stylesheet" type="text/css" media="screen and (max-width: 700px)" href="{{ asset('main/app/css/mobile.css') }}" />
		<link rel="stylesheet" type="text/css" media="screen and (min-width: 701px) and (max-width: 799px)" href="{{ asset('main/app/css/tablet.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('main/css/table.css') }}" />
	</head>
	<body>
		<header class="w100">
			<div class="notif w100 taCenter">
				<div class="container">
					<div class="notif-wrapper">
						<section class="news-bar">
							<div class="first">
								<dl id="ticker-1">
								    <dt>Berita Terbaru</dt>
								        <dd>Nikmati komisi live casino terbesar sampai dengan 1,2%</dd>
							        <dt>Berita Terbaru</dt>
								        <dd>Komisi Casino Terbesar Sampai Dengan 1.2%</dd>
							        <dt>Berita Terbaru</dt>
								        <dd>Mix Parlay Extra Bonus</dd>
								    <dt>Berita Terbaru</dt>
								        <dd>Promo Bonus 10% setiap deposit sportsbook</dd>
								    <dt>Berita Terbaru</dt>
								        <dd>Member Get Member Promo</dd>
								    <dt>Berita Terbaru</dt>
								        <dd>Cashback for casino 3%</dd>
								    <dt>Berita Terbaru</dt>
								        <dd>CashBack Sportsbook 5%</dd>
								</dl>
							</div>
		                </section>
						<div class="clearBoth"></div><!-- clearBoth -->
					</div><!-- notif-wrapper -->
				</div><!-- container -->
			</div><!-- notif -->
			<div class="header-bottom w100">
				<div class="container positionRelative">
					<div class="logo-agenwin floatLeft">
						<a href="/">
							<img class="img-logo-agenwin" src="{{ asset('main/app/img/logo/logo-agenwin.png') }}" width="217" height="79" />
						</a>
					</div><!-- logo-agenwin -->
					@include('layouts.app.menu')
				</div><!-- container -->
			</div><!-- header-bottom -->
		</header><!-- header -->
		<main class="w100">

			@yield('contents')

		</main><!-- main -->
		<footer class="w100">
			<div class="bank w100 taCenter">
				<div class="container">
					<div class="bank-wrapper">
						<div class="bank-title floatLeft taCenter bank-items">
							jam offline bank
						</div><!-- bank-title -->
						<div class="bank-bca floatLeft bank-items cursorPointer">
							<img class="img-bank-bca" src="{{ asset('main/app/img/logo/logo-bca.png') }}" width="114" height="93" />
						</div><!-- bank-bca -->
						<div class="bank-mandiri floatLeft bank-items cursorPointer">
							<img class="img-bank-mandiri" src="{{ asset('main/app/img/logo/logo-mandiri.png') }}" width="147" height="93" />
						</div><!-- bank-mandiri -->
						<div class="bank-bni floatLeft bank-items cursorPointer">
							<img class="img-bank-bni" src="{{ asset('main/app/img/logo/logo-bni.png') }}" width="100" height="93" />
						</div><!-- bank-bni -->
						<div class="bank-bri floatLeft bank-items cursorPointer">
							<img class="img-bank-bri" src="{{ asset('main/app/img/logo/logo-bri.png') }}" width="148" height="93" />
						</div><!-- bank-bri -->
						<div class="clearBoth"></div><!-- clearBoth -->
					</div><!-- bank-wrapper -->
				</div><!-- container -->
			</div><!-- bank -->

			@include('layouts.app.contact')

			<div class="footer-content w100">
				<div class="container">
					<div class="footer-content-top w100">
						<div class="footer-content-top-box-wrapper">
							<div class="footer-content-top-title floatLeft">
								<div class="footer-content-top-title-1 taCenter">
									24/7 support
								</div><!-- footer-content-top-title-1 -->
								<div class="footer-content-top-title-2 taCenter">
									online chat with us
								</div><!-- footer-content-top-title-2 -->
							</div><!-- footer-content-top-title -->
						</div><!-- footer-content-top-box-wrapper -->
						<div class="footer-content-top-box-wrapper">
							<div class="footer-content-top-ym taCenter cursorPointer footer-content-top-box floatLeft">
								<a href="ymsgr:sendIM?agenwin">
									chat via Yahoo messenger
								</a>
							</div><!-- footer-content-top-ym -->
						</div><!-- footer-content-top-box-wrapper -->
						<div class="footer-content-top-box-wrapper">
							<div class="footer-content-top-livechat taCenter cursorPointer footer-content-top-box floatLeft">
								<a href="#">
									chat via livechat online
								</a>
							</div><!-- footer-content-top-livechat -->
						</div><!-- footer-content-top-box-wrapper -->
						<div class="footer-content-top-box-wrapper">
							<div class="footer-content-top-fb taCenter cursorPointer footer-content-top-box floatLeft">
								<a href="#">
									<img class="footer-content-top-icon-fb" src="{{ asset('main/app/img/icon/icon-fb.png') }}" />
								</a>
							</div><!-- footer-content-top-fb -->
							<div class="footer-content-top-twitter taCenter cursorPointer footer-content-top-box floatLeft">
								<a href="#">
									<img class="footer-content-top-icon-twitter" src="{{ asset('main/app/img/icon/icon-twitter.png') }}" />
								</a>
							</div><!-- footer-content-top-twitter -->
						</div><!-- footer-content-top-box-wrapper -->
						<div class="clearBoth"></div><!-- clearBoth -->
					</div><!-- footer-content-top -->
					<div class="footer-content-bottom w100 taCenter">
						COPYRIGHT &copy; 2016 123judi - ALL RIGHTS RESERVED
					</div><!-- footer-content-bottom -->
				</div><!-- container -->
			</div><!-- footer-content -->
		</footer><!-- footer -->

		{{-- <script type="text/javascript" async="" src="http://cdn.livechatinc.com/tracking.js"></script> --}}
		<script type="text/javascript" src="{{ asset('main/app/js/dist/all.js') }}"></script>
		<script type="text/javascript" src="{{ asset('main/app/js/main.js') }}"></script>
		<script type="text/javascript">
          $(function() {
            var _scroll = {
              delay: 1000,
              easing: 'linear',
              items: 1,
              duration: 0.07,
              timeoutDuration: 0,
              pauseOnHover: 'immediate'
            };
            $('#ticker-1').carouFredSel({
              width: 980,
              align: false,
              items: {
                width: 'variable',
                height: 35,
                visible: 1
              },
              scroll: _scroll
            });

            //  set carousels to be 100% wide
            $('.caroufredsel_wrapper').css('width', '100%');

            //  set a large width on the last DD so the ticker won't show the first item at the end
            // $('#ticker-2 dd:last').width(2000);
          });
        </script>

		<!-- Start of LiveChat (www.livechatinc.com) code -->
		<script type="text/javascript">
			window.__lc = window.__lc || {};
			window.__lc.license = 7544191;
			(function() {
			  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
			  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
			  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
			})();
		</script>
{{-- 		<iframe src="https://secure.livechatinc.com/licence/7544191/open_chat.cgi" frameborder="0" scrolling="no" width="370" height="435" align="right" style="position: fixed; bottom: 0; right: 20px;"></iframe> --}}
		<!-- End of LiveChat code -->


		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-78891714-1', 'auto');
		  ga('send', 'pageview');
		</script>

		<script type="text/javascript">var _Hasync= _Hasync|| [];_Hasync.push(['Histats.start', '1,3467172,4,443,112,61,00011001']);_Hasync.push(['Histats.fasi', '1']);_Hasync.push(['Histats.track_hits', '']);(function() {var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;hs.src = ('//s10.histats.com/js15_as.js');(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);})();
		</script>
		<noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?3467172&101" alt="hit tracker" border="0"></a></noscript><!-- Histats.com  END  -->
		@yield('footer')

		<div class="back-black positionAbsolute"></div>
		<div class="popup positionAbsolute"></div>
	</body>
</html>