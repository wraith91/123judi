<div class="box-contact positionAbsolute cursorPointer">
	<div class="box-contact-wa box-contact-content floatRight">
		<img class="floatLeft" src="{{ asset('main/app/img/icon/icon-wa.png') }}" width="64" height="64" />
		<div class="box-contact-text floatLeft">+6282279123123
		<span>WhatsApp</span>
		</div><!-- box-contact-text -->
	</div><!-- box-contact-wa -->
	<div class="clearBoth"></div><!-- clearBoth -->
	<div class="box-contact-bbm box-contact-content floatRight">
		<img class="floatLeft" src="{{ asset('main/app/img/icon/icon-bbm.png') }}" width="64" height="64" />
		<div class="box-contact-text floatLeft">2B00B5EB
		<span>BBM</span>
		</div><!-- box-contact-text -->
	</div><!-- box-contact-bbm -->
	<div class="clearBoth"></div><!-- clearBoth -->
	<div class="box-contact-chat box-contact-content floatRight">
		<img class="floatLeft" src="{{ asset('main/app/img/icon/icon-chat.png') }}" width="64" height="64" />
		<div class="box-contact-text floatLeft">+6282279123123
		<span>SMS</span>
		</div><!-- box-contact-text -->
	</div><!-- box-contact-chat -->
	<div class="clearBoth"></div><!-- clearBoth -->
	<div class="box-contact-ym box-contact-content floatRight">
		<img class="floatLeft" src="{{ asset('main/app/img/icon/icon-ym.png') }}" width="64" height="64" />
		<div class="box-contact-text floatLeft">INDOBET123
		<span>Yahoo Messenger</span>
		</div><!-- box-contact-text -->
	</div><!-- box-contact-chat -->
	<div class="clearBoth"></div><!-- clearBoth -->
</div><!-- box-contact -->
<div class="box-contact-mobile w100">
	<div class="box-contact-wa-mobile w100 box-contact-content-mobile">
		<div class="box-contact-mobile-wrapper">
			<img class="floatLeft" src="{{ asset('main/app/img/icon/icon-wa.png') }}" width="64" height="64" />
			<div class="box-contact-text-mobile floatLeft">
				<a href="intent://send/+082279123123#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end">
					+6282279123123
				</a>
			</div><!-- box-contact-text -->
		</div><!-- box-contact-mobile-wrapper -->
	</div><!-- box-contact-wa -->
	<div class="box-contact-bbm-mobile w100 box-contact-content-mobile">
		<div class="box-contact-mobile-wrapper">
			<img class="floatLeft" src="{{ asset('main/app/img/icon/icon-bbm.png') }}" width="64" height="64" />
			<div class="box-contact-text-mobile floatLeft">
				<a href="pin:2B00B5EB">
				 	2B00B5EB
				<a>
			</div><!-- box-contact-text -->
		</div><!-- box-contact-mobile-wrapper -->
	</div><!-- box-contact-bbm -->
	<div class="box-contact-chat-mobile w100 box-contact-content-mobile">
		<div class="box-contact-mobile-wrapper">
			<img class="floatLeft" src="{{ asset('main/app/img/icon/icon-chat.png') }}" width="64" height="64" />
			<div class="box-contact-text-mobile floatLeft">
				<a href="sms:082279123123">
					+6282279123123
				</a>
			</div><!-- box-contact-text -->
		</div><!-- box-contact-mobile-wrapper -->
	</div><!-- box-contact-chat -->
	<div class="box-contact-chat-mobile w100 box-contact-content-mobile">
		<div class="box-contact-mobile-wrapper">
			<img class="floatLeft" src="{{ asset('main/app/img/icon/icon-ym.png') }}" width="64" height="64" />
			<div class="box-contact-text-mobile floatLeft">
				<a href="email:INDOBET123">
					INDOBET123
				</a>
			</div><!-- box-contact-text -->
		</div><!-- box-contact-mobile-wrapper -->
	</div><!-- box-contact-ym -->
</div><!-- box-contact -->