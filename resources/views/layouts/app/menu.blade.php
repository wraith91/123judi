<div class="hamburger-menu positionAbsolute cursorPointer">
	<div class="hamburger-menu-item"></div>
	<div class="hamburger-menu-item"></div>
	<div class="hamburger-menu-item"></div>
</div><!-- hamburger-menu -->
<div class="menu-content-mobile positionAbsolute w100 taCenter">
	<div class="menu-triangle positionAbsolute"></div><!-- menu-triangle -->
	<ul class="menu-mobile">
		<li>
			<a href="/">
				home
			</a>
		</li>
		<li>
			<a href="{{ URL::to('casino') }}">
				casino
			</a>
		</li>
		<li>
			<a href="{{ URL::to('poker') }}">
				poker
			</a>
		</li>
		<li>
			<a href="{{ URL::to('games') }}">
				games
			</a>
		</li>
		<li>
			<a href="{{ URL::to('/promo') }}">
				promo
			</a>
		</li>
		<li>
			<a href="{{ URL::route('member::transaction:deposit') }}">
				deposit
			</a>
		</li>
		<li>
			<a href="{{ URL::route('member::transaction:withdraw') }}">
				withdraw
			</a>
		</li>
		<li>
			<a href="{{ URL::to('help') }}">
				help
			</a>
		</li>
		<li>
			<a href="{{ URL::to('tebakbola') }}">
				tebakbola
			</a>
		</li>
		<li>
			<a class="link-login menu-register-link" href="{{ URL::to('signin') }}">
				login
			</a>
		</li>
		<li>
			<a class="link-register menu-register-link" href="{{ URL::to('register') }}">
				register
			</a>
		</li>
	</ul><!-- menu-mobile -->
</div><!-- menu-content-mobile -->
<div class="menu-content floatLeft">
	<ul class="menu">
		<li class="{{ set_active('/') }}">
			<a href="/">
				home
			</a>
		</li>
		<li class="{{ set_active(['casino', 'casino-*']) }}">
			<a href="{{ URL::to('casino') }}">
				casino
			</a>
		</li>
		<li class="{{ set_active('poker') }}">
			<a href="{{ URL::to('poker') }}">
				poker
			</a>
		</li>
		<li class="{{ set_active(['games', 'game-*']) }}">
			<a href="{{ URL::to('games') }}">
				games
			</a>
		</li>
		<li class="{{ set_active('promo') }}">
			<a href="{{ URL::to('/promo') }}">
				promo
			</a>
		</li>
		<li class="{{ set_active('transaction/deposit') }}">
			<a href="{{ URL::route('member::transaction:deposit') }}">
				deposit
			</a>
		</li>
		<li class="{{ set_active('transaction/withdraw') }}">
			<a href="{{ URL::route('member::transaction:withdraw') }}">
				withdraw
			</a>
		</li>
		<li class="{{ set_active('help') }}">
			<a href="{{ URL::to('help') }}">
				help
			</a>
		</li>
		<li class="{{ set_active('tebakbola') }}">
			<a href="{{ URL::to('tebakbola') }}">
				Tebak Bola
			</a>
		</li>
	</ul><!-- menu -->
</div><!-- menu-content -->
@if (Auth::guest())
	<div class="menu-register floatLeft">
		<a class="link-login menu-register-link" href="{{ URL::to('signin') }}">login</a>
		<a class="link-register menu-register-link" href="{{ URL::to('register') }}">register</a>
	</div><!-- menu-register -->
@else
	<div class="menu-register floatLeft">
		<a class="link-register menu-register-link" href="/{{ Auth::user()->username  }}">Accnt</a>
		<a class="link-register menu-register-link" href="{{ URL::to('auth/logout') }}">logout</a>
	</div>
@endif