var elixir = require('laravel-elixir');
require('laravel-elixir-sass-compass');
require('laravel-elixir-imagemin');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.images = {
    folder: 'images',
    outputFolder: 'main/images',
    optimizationLevel: 3,
    progressive: true,
    interlaced: true
};

elixir(function(mix) {

    mix.compass('admin.scss', 'public/main/css/', {
      sass: "resources/assets/sass",
      font: "public/main/fonts",
      image: "public/main/images",
      javascript: "public/main/js",
      sourcemap: false
    })
    .compass('styles.scss', 'public/main/css/', {
      sass: "resources/assets/sass",
      font: "public/main/fonts",
      image: "public/main/images",
      javascript: "public/main/js",
      sourcemap: false
    })
    .compass('table.scss', 'public/main/css/', {
      sass: "resources/assets/sass",
      font: "public/main/fonts",
      image: "public/main/images",
      javascript: "public/main/js",
      sourcemap: false
    })
    .imagemin()

    .sass('general.scss', 'public/main/app/css')

    .copy(
        [
          'resources/assets/components/site-newdesign/css/tablet.css',
          'resources/assets/components/site-newdesign/css/mobile.css'
        ],
        'public/main/app/css'
    )
    .copy(
        'resources/assets/components/site-newdesign/js',
        'public/main/app/js'
    )
    .copy(
        'resources/assets/components/site-newdesign/img',
        'public/main/app/img'
    )

    /*
     * Copying Vendor Javascripts
     */
    .copy(
        'resources/assets/js/ajax-script.js',
        'public/main/js/'
    )
    .copy(
        'resources/assets/components/jquery/dist/jquery.js',
        'public/main/js/vendor'
    )
    .copy(
        'resources/assets/js/iconic.min.js',
        'public/main/js/vendor'
    )
    .copy(
        'resources/assets/js/jquery.carouFredSel.js',
        'public/main/app/js'
    )

    /*
     * Concatenation of CSS Files
     */

    .styles([
        'tooltipster.css',
        'slick.css',
        'flexslider.css',
        'slick-theme.css',
        'mCustomScrollbar.css',
    ], 'public/main/app/css/site-features.css', 'resources/assets/components/site-newdesign/css')

    .styles([
        'resources/assets/components/site-newdesign/css/desktop.css',
        'resources/assets/css/runningtext.css',
    ], 'public/main/app/css/desktop.css', './')



    /*
     * Concatenation of JS Files
     */

    .scripts([
        'jquery.js',
        'iconic.min.js',
    ], 'public/main/js/vendor/admin', 'public/main/js/vendor')

    .scripts([
        'jquery.1.12.0.min.js',
        'jquery.ticker.min.js',
        'jquery.slick.min.js',
        'jquery.tooltipster.min.js',
        'jquery.responsiveCarousel.min.js',
        'jquery.flexslider.min.js',
        'jquery.mCustomScrollbar.concat.min.js',
        'jquery.carouFredSel.js',
    ], 'public/main/app/js/dist/all.js', 'public/main/app/js');
});
