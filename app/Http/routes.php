<?php

// Event::listen('illuminate.query', function($query)
// {
//     var_dump($query);
// });

Route::group(['middleware' => 'user'], function () {
/**
 * Application Static Routes
 */
Route::get('/', ['as' => 'home', 'uses' => 'PagesController@home']);
Route::get('/casino', 'PagesController@casino');
Route::get('/casino-sbobet', 'PagesController@casinoSbobet');
Route::get('/casino-calibet', 'PagesController@casinoCalibet');
Route::get('/casino-ibcbet', 'PagesController@casinoIbcbet');
Route::get('/games', 'PagesController@games');
// Route::get('/game-sabung', 'PagesController@gameSabung');
Route::get('/poker', 'PagesController@poker');
Route::get('/promo', 'PagesController@promo');
Route::get('/tebakbola', 'PagesController@tebakbola');
Route::get('/help', 'PagesController@help');
Route::get('/contact', 'PagesController@contact');
Route::post('/contact', 'InquiryController@store');
Route::get('/auth/login', 'PagesController@login');

Route::get('/news', 'App\ArticleController@index');
Route::get('/news/{title}', 'App\ArticleController@show');

});

/**
 * Registration
 */

Route::get('register', ['as' => 'register', 'uses' => 'RegistrationController@index', 'middleware' => 'guest']);
Route::post('register', ['as' =>'registration.store', 'uses' => 'RegistrationController@store']);

/**
 * Authentication
 */

Route::get('auth/login', ['as' => 'auth/login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('auth/login', ['as' => 'auth.postLogin', 'uses' =>  'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' =>  'Auth\AuthController@getLogout', 'middleware' => 'auth']);

Route::resource('sessions', 'SessionsController', ['only' => ['create', 'store']]);
Route::resource('admin/auth/login', 'AdminController', ['only' => ['create', 'store']]);
Route::get('login', ['as' => 'login', 'uses' => 'SessionsController@create']);
Route::get('admin/auth/login', ['as' => 'admin.auth.login', 'uses' => 'AdminController@create', 'middleware' => 'restricted']);



/**
 * -------------------------------------------
 * Member function
 * -------------------------------------------
 */

Route::group(['as' => 'member::', 'middleware' => 'auth'], function () {

	/**
	 * User Transaction
	 */

	Route::get('transaction/deposit', ['as' => 'transaction:deposit', 'uses' => 'TransactionController@deposit']);
	Route::get('transaction/withdraw', ['as' => 'transaction:withdraw', 'uses' => 'TransactionController@withdraw']);
	Route::get('transaction/history', ['as' => 'transaction:history', 'uses' => 'TransactionController@index']);
	Route::post('transaction', ['as' => 'transaction:create', 'uses' => 'TransactionController@store']);
	Route::patch('transaction/withdraw/{id}', ['as' => 'transaction/withdraw/{id}', 'uses' => 'TransactionController@update']);
	Route::get('game/account/{id}', 'UserGameProfileController@show');

	/**
	 * User Profile
	 */
	Route::resource('profile', 'ProfilesController', ['only' => ['update', 'show']]);
	Route::get('/{profile}', ['as' => 'profile', 'uses' => 'ProfilesController@show']);
	Route::put('/{profile}', ['as' => 'profile.update', 'uses' => 'ProfilesController@update']);
	Route::get('/{profile}/settings', 'ProfilesController@settings');
	Route::patch('/settings/{id}', 'ProfilesController@updatePassword');

});

/**
 * -------------------------------------------
 * Administrator function
 * -------------------------------------------
 */

Route::group(['as' => 'admin::', 'middleware' => ['auth', 'authorized']], function () {
  /**
	 * Dashboard
	 */
  Route::get('admin/dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);

  	/**
	 * Member
	 */
	Route::get('admin/members/lists', 'Member\MemberController@lists');
	Route::get('admin/members/{id}/edit', 'Member\MemberController@edit');
	Route::patch('admin/members/{id}', 'Member\MemberController@update');
	Route::patch('admin/members/bankaccount/{id}', 'Member\MemberBankAccountController@update');
	Route::delete('admin/members/{id}', ['as' => 'member:delete', 'uses' => 'Member\MemberController@destroy']);
	Route::get('admin/members/information', ['as' => 'members/information', 'uses' => 'Member\MemberController@index']);


	Route::post('admin/transactions/gameprofile/{id}', 'UserGameProfileController@store');
	Route::get('admin/members/information/account/{name}/{id}', 'Member\MemberController@show');

	Route::get('admin/members/game-id-lists/export', 'Member\MemberGameAccountController@export');
	Route::resource('admin/members/game-id-lists', 'Member\MemberGameAccountController', ['only' => ['index', 'show']]);

 	/**
	 * Transaction
	 */
	Route::get('admin/transactions/deposit/export', 'DepositController@export');
	Route::get('admin/transactions/withdraw/export', 'WithdrawController@export');
  	Route::resource('admin/transactions/deposit', 'DepositController', ['only' => ['index', 'show', 'update']]);
	Route::resource('admin/transactions/withdraw', 'WithdrawController', ['only' => ['index', 'show', 'update']]);
	Route::get('admin/transactions/log', 'GameTransactionLogController@index');
	Route::post('admin/transactions/log/{id}', 'GameTransactionLogController@store');


	Route::get('admin/bonus/history/{id}', 'Bonus\BonusController@index');
	Route::get('admin/bonus/game/{id}', 'Bonus\BonusController@show');
	Route::patch('admin/bonus/game/{id}', 'Bonus\BonusController@update');
	Route::post('admin/bonus/upload', 'Bonus\BonusController@upload');

  	/**
	 * Inquiry
	 */
	Route::resource('admin/inquiry', 'InquiryController');

  	/**
	 * Article
	 */
	Route::resource('admin/article', 'ArticlesController');

	/**
	 * Account
	 */
	Route::get('admin/account/settings', 'AccountSettingsController@create');
	Route::patch('admin/account/settings/{id}', 'AccountSettingsController@update');

	Route::group(['middleware' => 'admin'], function () {
		Route::resource('admin/system/user', 'AuthorizeUserController');
	});

});