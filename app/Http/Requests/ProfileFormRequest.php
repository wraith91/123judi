<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

class ProfileFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $rules = [
        'first_name'            => 'required',
        // 'last_name'             => 'required',
        // 'birth_date'            => 'required',
        // 'gender'                => 'required',
        'mobile'                => 'min:10',
        ];
    }
}
