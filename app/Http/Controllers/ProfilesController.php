<?php

namespace App\Http\Controllers;

use App\Agenwin\User;
use App\Agenwin\BankType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\ProfileFormRequest;
use Illuminate\Support\Facades\Hash;
use Auth;

class ProfilesController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  $username
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileFormRequest $request, $username)
    {
        $user = $this->user->whereUsername($username)->firstOrFail();
        
        $input = $request->except('_method', '_token');

        if($user->profile->fill($input)->save() || $user->bankAccount->fill($input)->save())
        {
            session()->flash('flash_message', 'Your profile has been updated');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  $username
     * @return view
     */
    public function show($username)
    {
        try 
        {
            $user = $this->user->with('profile', 'bankAccount')->whereUsername($username)->firstOrFail();
            
            $bank_name = BankType::lists('name', 'id');
        }
        catch(ModelNotFoundException $e)
        {
            return redirect()->home();
        }

        return view('app.profile.show', compact('user', 'bank_name'));
    }

    public function settings($username)
    {
        return view('app.profile.setting');
    }

    public function updatePassword(Request $request, $id)
    {
        $this->validate($request, ['old_password' => 'required|min:8', 'password' => 'min:8|confirmed', 'password_confirmation' => 'min:8']);

        if (!Hash::check($request->old_password, auth()->user()->password))              //check what bank is submitted
        {
            return redirect()->back()->withErrors('Old password does not exist.');   //apply custom rule for each bank
        }
      
        $user = $this->user->findOrFail($id);

        if ($user->fill($request->except('_method', '_token'))->save())
        {
            session()->flash('flash_message', 'Change password successfully!');
        }

        return redirect()->back();
    }
}
