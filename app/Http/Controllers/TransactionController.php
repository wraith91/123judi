<?php

namespace App\Http\Controllers;

use App\Agenwin\User;
use App\Agenwin\TransactionType;
use App\Agenwin\Transaction;
use App\Agenwin\TransactionStatus;
use App\Agenwin\GameType;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    protected $transaction;
    protected $user;

    public function __construct(Transaction $transaction, User $user)
    {
        $this->transaction = $transaction;
        $this->user = $user;
    }



    public function index(Request $request = null)
    {
        if ($request->get('trxn_type_id'))
        {
            $history = $this->transaction->where('user_id', auth()->user()->id);

            if ($request->get('trxn_type_id') == 1)
            {
                $transactions = $history->deposit();
            }
            else if ($request->get('trxn_type_id') == 2)
            {
                $transactions = $history->withdraw();
            }

            $transactions = $history->get();
        }
        else 
        {
            $transactions = null;
        }

        return view('app.transaction.history', compact('transactions'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       
        $this->validate($request, ['amount' => 'required|integer|min:50', 'trxn_type_id' => 'required|integer|max:2|min:1']);

        if ($request->trxn_type_id == 2)
        {
            $negative = 0;

            $negative -= $request->amount;

            $request->offsetSet('amount', $negative);
        }

        $id = $request->trxn_user_id;

        if (!$request->trxn_user_id)
        {
            $id = auth()->user()->id;
        }

        $user = $this->user->with('bankAccount')->find($id);

        $transaction = new Transaction([
            'trxn_type_id' => $request->trxn_type_id,
            'bank_id' => $user->bankAccount->id,
            'game_id' => $request->game_id,
            'amount' => $request->amount
        ]);
       
        if($user->transaction()->save($transaction))
        {
            session()->flash('flash_message', 'Your transaction has been created');
        }

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaction = $this->transaction->findOrFail($id);

        if ($transaction->trxn_status_id > 2)
        {
            return redirect()->back()->withErrors('Your transaction is currently on process..');
        }
        
        $transaction_status = TransactionStatus::where('name', 'Cancelled')->first();

        if ($transaction->fill([
                'trxn_status_id' => $transaction_status->id, 
                'remarks' => 'cancelled by the user', 
                'modify_by' => auth()->user()->username
            ])->save())
        {
            session()->flash('flash_message', 'Transaction has been cancelled');
        }

        return redirect()->back();
    }

    /**
     * Display deposit page
     *
     * @return \Illuminate\Http\Response
     */
    public function deposit(GameType $game_type)
    {
        try 
        {
            $user = $this->user->with(['bankAccount', 'transaction.gameType', 'transaction.status' ,'transaction' => function($query)
                        {
                            $query->where('trxn_type_id', '1')->whereIn('trxn_status_id', ['2', '4'])->orderBy('id', 'desc')->take(6);
                        }
                    ])->find(auth()->user()->id);

            $game = $game_type->lists('name', 'id');
        }
        catch(ModelNotFoundException $e)
        {
            return redirect()->home();
        }

        // return $user->transaction; 
        if (in_array_r('Pending', $user->transaction->toArray()))
        {
            session()->flash('info_message', 'You still have pending deposit transaction.');
        }

        return view('app.transaction.deposit', compact('user', 'game'));
    }

    /**
     * Display withdraw page
     *
     * @return \Illuminate\Http\Response
     */
    public function withdraw()
    {
        try 
        {
            $user = $this->user->with(['transaction.status', 'gameAccount.gameType', 'transaction' => function($query)
                        {
                            $query->where('trxn_type_id', '2')->whereNotIn('trxn_status_id', ['1', '3'])->orderBy('id', 'desc')->take(6);
                        }
                    ])->find(auth()->user()->id);
        }
        catch(ModelNotFoundException $e)
        {
            return redirect()->home();
        }
        
        foreach ($user->gameAccount as $types)
        {
            $type[$types->gameType->id] = $types->gameType->name;
        }

        return view('app.transaction.withdraw', compact('user', 'type'));
    }
}
