<?php

namespace App\Http\Controllers;

use App\Traits\CaptchaTrait;
use App\Agenwin\Inquiry;
use App\Agenwin\InquiryStatus;

use App\Http\Requests\InquiryRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class InquiryController extends Controller
{
    use CaptchaTrait;

    protected $inquiry;
    protected $inquiry_status;

    public function __construct(Inquiry $inquiry, InquiryStatus $inquiry_status)
    {
        $this->inquiry = $inquiry;
        $this->inquiry_status = $inquiry_status;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inquiries = $this->inquiry->with(['type', 'status'])->pending()->orderBy('id', 'desc')->get();
        
        $status = $this->inquiry_status->whereNotIn('id', ['1'])->orderBy('id', 'asc')->lists('name', 'id');

        if (Input::get('id'))
        {
            $msg = $this->inquiry->where('id', Input::get('id'))->get();        
        }

        return view('admin.inquiry.index', compact('inquiries', 'msg', 'status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InquiryRequest $request)
    {
        if (auth()->check())
        {
            $request->offsetSet('name', auth()->user()->username);
            $request->offsetSet('email', auth()->user()->email);
        }

        if($this->captchaCheck() == false)
        {
            return redirect()->back()
                ->withErrors(['Wrong Captcha'])
                ->withInput();
        }


        if ($inquiry = $this->inquiry->create($request->except('_token'))) 
        {
            session()->flash('flash_message', 'Inquiry has been submitted.');
        }

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $inquiry = $this->inquiry->find($id);

        if ($inquiry->fill($request->except('_method', '_token'))->save()) 
        {
            $status = $this->inquiry->with('status')->select('id', 'inquiry_status_id')->find($id);
            session()->flash('flash_message', 'Inquiry has been ' . strtolower($status->status->name));
        }

        return redirect('admin/inquiry');
    }
}
