<?php

namespace App\Http\Controllers;

use App\Agenwin\User;
use App\Agenwin\TransactionStatus;
use App\Agenwin\Transaction;
use App\Agenwin\GameType;
use App\Agenwin\UserGameProfile;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class DepositController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request = null)
    {
        $status = TransactionStatus::orderBy('id', 'asc')->whereNotIn('id', ['5'])->lists('name', 'id')->toArray();

        $records = Transaction::with('status', 'user', 'bank.bankName', 'gameType')->orderBy('id', 'desc')->deposit();

        switch ($request->get('status')) {
            case 1:
                $transactions = $records;
                break;
            case 2:
                $transactions = $records->pending();
                break;
            case 3:
                $transactions = $records->approved();
                break;
            case 4:
                $transactions = $records->rejected();
                break;
            case 5:
                $transactions = $records->tobeapproved();
                break;
            case 6:
                $transactions = $records->processing();
                break;
            case 7:
                $transactions = $records->cancelled();
                break;
            default:
                $transactions = $records->pending();
                break;
        }

        if ($request->get('from') != null && $request->get('to') != null)
        {
            $transactions = $transactions->between($request->get('from'), $request->get('to'))->get();
        }

        $transactions = $records->get();

        return view('admin.deposit.index', compact('transactions', 'status'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::with('status', 'user.profile', 'bank.bankName', 'gameType')->find($id);

        try
        {
            $gameAccount = UserGameProfile::where('user_id', $transaction->user_id)->where('game_id', $transaction->game_id)->firstOrFail();
        }
        catch (ModelNotFoundException $e)
        {
            $gameAccount = [];
        }

        $status = TransactionStatus::whereIn('id', ['3', '4'])->orderBy('id','asc')->lists('name', 'id');

        $game = GameType::lists('name', 'id');

        // return $gameAccount;
        return view('admin.deposit.show', compact('transaction', 'status', 'game', 'gameAccount'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->trxn_status_id == 4)
        {
            $this->validate($request, ['remarks' => 'required']);
        }

        $request->merge(['modify_by' => auth()->user()->username, 'completed_at' => date('Y-m-d')]);

        $transaction = Transaction::findOrFail($id);

        if ($transaction->fill($request->except('_method', '_token'))->save())
        {
            session()->flash('flash_message', 'Transaction has been updated');
        }

        return redirect()->back();
    }

    public function export()
    {
       return Excel::create('Deposit Transactions', function($excel) {

            $excel->setTitle('Our new awesome title');

            $excel->sheet('Sheet 1', function($sheet) {

                $sheet->setFontFamily('Comic Sans MS');

                $records = Transaction::with('status', 'user', 'bank.bankName', 'gameType')->orderBy('id', 'desc')->deposit();

                switch (Input::get('status')) {
                    case 1:
                        $transactions = $records;
                        break;
                    case 2:
                        $transactions = $records->pending();
                        break;
                    case 3:
                        $transactions = $records->approved();
                        break;
                    case 4:
                        $transactions = $records->rejected();
                        break;
                    case 5:
                        $transactions = $records->tobeapproved();
                        break;
                    case 6:
                        $transactions = $records->processing();
                        break;
                    case 7:
                        $transactions = $records->cancelled();
                        break;
                    default:
                        $transactions = $records->pending();
                        break;
                }

                if (Input::get('from') != null && Input::get('to') != null)
                {
                    $transactions = $transactions->between(Input::get('from'), Input::get('to'))->get();
                }

                $transactions = $records->get();

                $sheet->loadView('admin.deposit.xls')->with('transactions', $transactions);

            });

        })->export('xls');
    }
}