<?php

namespace App\Http\Controllers\Bonus;

use App\Agenwin\UserGameProfile;
use App\Agenwin\BonusStatus;
use App\Agenwin\Bonus;
use App\Agenwin\BonusFileLog;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Input;

class BonusController extends Controller
{
    protected $bonus;

    public function __construct(Bonus $bonus)
    {
        $this->bonus = $bonus;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        if ($id) {

            switch ($id) 
            {
                case 1:
                    $logs = BonusFileLog::sbo();
                    break;
                case 2:
                    $logs = BonusFileLog::ibc();
                    break;
                case 3:
                    $logs = BonusFileLog::cali();
                    break;
                case 4:
                    $logs = BonusFileLog::klik();
                    break;
                
                default:
                    $logs = $logs->lists('name', 'id');
                    break;
            }
        }

        $logs = $logs->lists('name', 'id');
        
        $records = $this->bonus->transferred()->file(Input::get('file'))->orderBy('id', 'asc')->get();

        return view('admin.bonus.history.index', compact('records', 'logs', 'id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($id) {

            $records = $this->bonus->with('gameAccount.user', 'gameAccount', 'status', 'gameType')->nottransferred();

            switch ($id) 
            {
                case 1:
                    $records = $records->sbo();
                    break;
                case 2:
                    $records = $records->ibc();
                    break;
                case 3:
                    $records = $records->cali();
                    break;
                case 4:
                    $records = $records->klik();
                    break;
                
                default:
                    $records = $records->sbo();
                    break;
            }
        }

        $records = $records->get();

        return view('admin.bonus.upload.show', compact('records'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['balance' => 'required|numeric']);

        $bonus = $this->bonus->find($id);

        $request->offsetSet('bonus_status_id', 2);    
        if ($bonus->update($request->except('_token', '_method'))) 
        {
            session()->flash('flash_message', 'Bonus has been successfully transferred!');
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function upload()
    {
        if (Input::hasFile('fileToUpload'))
        {
            $file = Input::file('fileToUpload');
            
            Excel::load($file, function ($reader) { 

               $lists = $reader->get(); // Store the collection to be cast in array in the foreach loop

               $game_type_id = $reader->get()->first()->game_type_id; // Store the first row in lists to be used in BonusFileLog

               try {
                   $file_type = BonusFileLog::create(['name' => Input::file('fileToUpload')->getClientOriginalName(), 'game_type_id' => $game_type_id]);
               } catch (QueryException $e) {
                   return redirect()->back()->withErrors('This file has been uploaded!');
               }

               //  // insert new row to db
                foreach ($lists->toArray() as $row) {
                    $bonus = $this->bonus->create([
                        'game_profile_id' => $row['game_profile_id'],
                        'game_type_id' => $row['game_type_id'],
                        'bonus_status_id' => 1,
                        'bonus_point' => $row['point'],
                        'balance' => '',
                        'file_log_id' => $file_type->id
                    ]);
                }
            });
            return redirect()->back();
        }    
        return redirect()->back()->withErrors('No file chosen!');
    }
}


