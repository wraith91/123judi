<?php

namespace App\Http\Controllers;

use App\Agenwin\Article;
use App\Agenwin\GameType;
use App\Agenwin\InquiryType;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
	/**
	 * Show the application landing page
	 *
	 * @return Response
	 */
    public function home()
    {
        $articles = Article::orderBy('created_at', 'desc')->limit(4)->get();

		return view('app.index', compact('articles'));
    }

    /**
	 * Show the application registration page
	 *
	 * @return Response
	 */
    public function register()
    {
		return view('app.register');
    }

    /**
	 * Show the application casino page
	 *
	 * @return Response
	 */
    public function casino()
    {
		return view('app.casino');
    }

    /**
     * Show the application casino-sbobet page
     *
     * @return Response
     */
    public function casinoSbobet()
    {
        return view('app.casino-sbobet');
    }

    /**
     * Show the application casino-ion page
     *
     * @return Response
     */
    public function casinoIon()
    {
        return view('app.casino-ion');
    }

    /**
     * Show the application casino-ibcbet page
     *
     * @return Response
     */
    public function casinoIbcbet()
    {
        return view('app.casino-ibcbet');
    }

    /**
     * Show the application casino-calibet page
     *
     * @return Response
     */
    public function casinoCalibet()
    {
        return view('app.casino-calibet');
    }

    /**
     * Show the application games page
     *
     * @return Response
     */
    public function games()
    {
        return view('app.game-sabung');
    }

    /**
     * Show the application game-sabung page
     *
     * @return Response
     */
    public function gameSabung()
    {
        return view('app.game-sabung');
    }

    /**
	 * Show the application sportsbook page
	 *
	 * @return Response
	 */
    public function poker()
    {
		return view('app.poker');
    }

    /**
     * Show the application promo
     *
     * @return Response
     */
    public function promo()
    {
        return view('app.promo');
    }

    /**
     * Show the application tebakbola
     *
     * @return Response
     */
    public function tebakbola()
    {
        return view('app.tebakbola');
    }

    /**
     * Show the application help page
     *
     * @return Response
     */
    public function help()
    {
        return view('app.help');
    }

    /**
     * Show the application login page
     *
     * @return Response
     */
    public function login()
    {
        return view('app.login');
    }

    /**
	 * Show the application contact page
	 *
	 * @return Response
	 */
    public function contact()
    {
        $type = InquiryType::orderBy('id', 'asc')->lists('name', 'id');

		return view('app.contact', compact('type'));
    }
}
