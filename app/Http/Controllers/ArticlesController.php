<?php

namespace App\Http\Controllers;

use App\Agenwin\User;
use App\Agenwin\Article;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleFormRequest;

class ArticlesController extends Controller
{
    protected $user;
    protected $article;

    public function __construct(Article $article)
    {
        $this->user = auth()->user();
        $this->article = $article;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = $this->article->orderBy('created_at', 'desc')->get();

        return view('admin.content.article.index',compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.content.article.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleFormRequest $request)
    {
        $imageFile = $request->file('image');

        $inputs = $request->except('_token', 'image');

        if ($request->hasFile('image'))
        {
            $name = time() . '-' . $imageFile->getClientOriginalName();

            $path = $imageFile->move(public_path() . '/main/app/img/article', $name);

            $inputs = $inputs + ['image' => $name];
        }

        $article = new Article($inputs);

        $this->user->article()->save($article);

        session()->flash('flash_message', 'Blog has been created!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $article = $this->article->find($id);

        return view('admin.content.article.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = $this->article->find($id);

        $article->delete();

        session()->flash('flash_message', $article->title . ' article has been deleted!');

        return redirect()->back();
    }
}
