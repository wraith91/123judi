<?php

namespace App\Http\Controllers;

use App\Agenwin\User;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AccountSettingsController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.account.settings.create');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'old_password' => 'required',
            'password' => 'required|min:8',
            'confirm_password' => 'required|same:password'
        ]);

        $user = User::findOrFail($id);

        if (!Hash::check($request->old_password, auth()->user()->password))             
        {
            return redirect()->back()->withErrors('Old password does not exist.');   
        }

        if ($user->fill($request->except('_token', '_method', 'old_password'))->save())
        {
            session()->flash('flash_message', 'Your password was successfully changed!');
        }

        return redirect()->back();
    }
}
