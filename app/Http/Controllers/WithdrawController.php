<?php

namespace App\Http\Controllers;

use App\Agenwin\Transaction;
use App\Agenwin\TransactionStatus;
use App\Agenwin\UserGameProfile;
use App\Agenwin\GameTransactionLog;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use FatalErrorException;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;

class WithdrawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request = null)
    {
        $status = TransactionStatus::orderBy('id', 'asc')->lists('name', 'id')->toArray();

        $records = Transaction::with('status', 'user', 'bank.bankName')->orderBy('id', 'desc')->withdraw();

        switch ($request->get('status')) {
            case 1:
                $transactions = $records;
                break;
            case 2:
                $transactions = $records->pending();
                break;
            case 3:
                $transactions = $records->approved();
                break;
            case 4:
                $transactions = $records->rejected();
                break;
            case 5:
                $transactions = $records->tobeapproved();
                break;
            case 6:
                $transactions = $records->processing();
                break;
            case 7:
                $transactions = $records->cancelled();
                break;
            default:
                $transactions = $records->pending();
                break;
        }

        if ($request->get('from') != null && $request->get('to') != null)
        {
            $transactions = $transactions->between($request->get('from'), $request->get('to'))->get();
        }

        $transactions = $records->get();

        return view('admin.withdraw.index', compact('transactions', 'status'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::with(['user.profile', 'bank.bankName', 'status'
                        ])->find($id);

        try
        {
            $gameAccount = UserGameProfile::where('user_id', $transaction->user_id)->where('game_id', $transaction->game_id)->firstOrFail();
            $game_log = GameTransactionLog::where('transaction_id', $id)->first();
        }
        catch (ModelNotFoundException $e)
        {
            $gameAccount = [];
        }
        catch(FatalErrorException $e)
        {
            $game_log = 0;
        }

        $status = TransactionStatus::whereIn('id', ['3','4','5','6'])->orderBy('id','asc')->lists('name', 'id');

        // return $status;
        return view('admin.withdraw.show', compact('transaction', 'status', 'gameAccount' ,'game_log'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['remarks' => 'required']);

        $request->merge(['modify_by' => auth()->user()->username, 'completed_at' => date('Y-m-d')]);

        $transaction = Transaction::findOrFail($id);

        if ($transaction->fill($request->except('_method', '_token'))->save())
        {
            session()->flash('flash_message', 'Transaction has been updated');
        }

        return redirect()->back();
    }

    /**
     * Export data to excel.
     *
     * @return Maatwebsite\Excel\Facades\Excel
     */
    public function export()
    {
       return Excel::create('Withdraw Transactions', function($excel) {

            $excel->sheet('Sheet 1', function($sheet) {

                $sheet->setFontFamily('Comic Sans MS');

                $records = Transaction::with('status', 'user', 'bank.bankName', 'gameType')->orderBy('id', 'desc')->withdraw();

                switch (Input::get('status')) {
                    case 1:
                        $transactions = $records;
                        break;
                    case 2:
                        $transactions = $records->pending();
                        break;
                    case 3:
                        $transactions = $records->approved();
                        break;
                    case 4:
                        $transactions = $records->rejected();
                        break;
                    case 5:
                        $transactions = $records->tobeapproved();
                        break;
                    case 6:
                        $transactions = $records->processing();
                        break;
                    case 7:
                        $transactions = $records->cancelled();
                        break;
                    default:
                        $transactions = $records->pending();
                        break;
                }

                if (Input::get('from') != null && Input::get('to') != null)
                {
                    $transactions = $transactions->between(Input::get('from'), Input::get('to'))->get();
                }

                $transactions = $records->get();

                $sheet->loadView('admin.withdraw.xls')->with('transactions', $transactions);

            });

        })->export('xls');
    }
}
