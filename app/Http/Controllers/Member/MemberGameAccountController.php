<?php

namespace App\Http\Controllers\Member;

use App\Agenwin\UserGameProfile;
use App\Agenwin\GameType;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class MemberGameAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.member.game_id_lists.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $records = UserGameProfile::with('gameType')->type($id)->get();

        return view('admin.member.game_id_lists.index', compact('records', 'id'));
    }

    public function export()
    {
        return Excel::create('Game Account Lists', function($excel) {

        $excel->sheet('Sheet 1', function($sheet) {

            $records = UserGameProfile::type(Input::get('id'))->get();

            $sheet->loadView('admin.member.game_id_lists.csv')->with('records', $records);

            });

        })->export('csv');
    }
}
