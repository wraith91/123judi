<?php

namespace App\Agenwin;

use Illuminate\Database\Eloquent\Model;

class InquiryType extends Model
{
		/**
	   * The database table used by the model.
	   *
	   * @var string
	   */
    protected $table = 'inquiry_types';

		/**
	   * Fillable fields for a InquiryType
	   *
	   * @var array
	   */
		protected $fillable = ['name'];

		/**
	   * The timestamp is disabled in this model.
	   *
	   * @var string
	   */
		public $timestamps = false;
}
