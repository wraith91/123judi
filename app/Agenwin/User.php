<?php

namespace App\Agenwin;


use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, HasRoles;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username', 'email', 'password', 'userlevel', 'ip', 'is_active'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that have their default value before stored in the database.
     *
     * @var array
     */
    protected $attributes = ['is_active' => 0];

    /**
     * Always hash password when we save it to the db
     * 
     * @param string
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    /**
     * Always set email name to lowercase when we save it to the db
     * 
     * @param string
     */
    public function setEmailAttribute($email)
    {
        $this->attributes['email'] = strtolower($email);
    }

    /**
     * User has only one profile.
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne(UserProfile::class);
    }

    /**
     * User can create many transactions.
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transaction()
    {
        return $this->hasMany(Transaction::class);
    }

    /**
     * User can create many articles.
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function article()
    {
        return $this->hasMany(Article::class);
    }

    /**
     * User has only one bank account.
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function bankAccount()
    {
        return $this->hasOne(UserBankProfile::class);
    }

    /**
     * User has many game accounts
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function gameAccount()
    {
        return $this->hasMany(UserGameProfile::class);
    }

    /**
     * User can receive many notifacations
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function credits()
    {
        return $this->transaction->where('trxn_status_id', '3')->sum('amount');
    }

    public function scopeSearch($query, $search)
    {
        return $query->whereUsername($search);
    }

    public function scopeLike($query, $search)
    {
        return $query->where('last_name', 'LIKE', "%$search%");
    }

    public function newNotification()
    {
        $notification = new App\Agenwin\Notification;
        $notification->user()->associate($this);
     
        return $notification;
    }

    public function isAMember()
    {
        return $this->hasRole('member');
    }

    public function isAdmin()
    {
        return $this->hasRole('admin');
    }

    public function isCS()
    {
        return $this->hasRole('cs');
    }
}
