<?php

namespace App\Agenwin;

use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
		/**
	   * The database table used by the model.
	   *
	   * @var string
	   */
    protected $table = 'inquiries';

		/**
	   * Fillable fields for a InquiryType
	   *
	   * @var array
	   */
		protected $fillable = ['name', 'email', 'inquiry_type_id', 'inquiry_status_id', 'subject', 'body'];

		protected $attributes = ['inquiry_status_id' => 1];

    /**
     * Always set email name to lowercase when we save it to the db
     * 
     * @param string
     */
    public function setEmailAttribute($email)
    {
        $this->attributes['email'] = strtolower($email);
    }

		public function type()
		{
				return $this->hasOne(InquiryType::class, 'id', 'inquiry_type_id');
		}

		public function status()
		{
				return $this->hasOne(InquiryStatus::class, 'id', 'inquiry_status_id');
		}

    /**
     * Scope a query to only include pending transactions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePending($query)
    {
        return $query->where('inquiry_status_id', 1);
    }

}
