<?php

namespace App\Agenwin;

use Illuminate\Database\Eloquent\Model;

class TransactionStatus extends Model
{
  	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transaction_status';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
	   * The timestamp is disabled in this model.
	   *
	   * @var string
	   */
    public $timestamps = false;

    public function transaction()
    {
        return $this->belongsTo('App\Agenwin\Transaction', 'id', 'trxn_status_id');
    }
}
