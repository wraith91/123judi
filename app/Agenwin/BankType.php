<?php

namespace App\Agenwin;

use Illuminate\Database\Eloquent\Model;

class BankType extends Model
{
		/**
	   * The database table used by the model.
	   *
	   * @var string
	   */
		protected $table = 'bank_types';

		/**
	   * Fillable fields for a banktype
	   *
	   * @var array
	   */
		protected $fillable = ['name'];

		/**
	   * The timestamp is disabled in this model.
	   *
	   * @var string
	   */
		public $timestamps = false;

		/**
     * Each BankType is belongs to UserBankProfile.
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bankProfile()
    {
    		return $this->belongsTo('App\Agenwin\UserBankProfile', 'id', 'bank_id');
    }
}
