<?php

namespace App\Agenwin;

use Illuminate\Database\Eloquent\Model;

class BonusFileLog extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bonus_file_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'game_type_id'];

    public function gameType()
    {
        return $this->hasOne(GameType::class, 'id', 'game_type_id');
    }

    public function scopeSbo($query)
    {
        return $query->where('game_type_id', 1);
    }

    public function scopeIbc($query)
    {
        return $query->where('game_type_id', 2);
    }

    public function scopeCali($query)
    {
        return $query->where('game_type_id', 3);
    }

    public function scopeKlik($query)
    {
        return $query->where('game_type_id', 4);
    }
}
